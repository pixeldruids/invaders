using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class RandomGenerator : MonoBehaviour {
	
	public static string randomVal = "";
	//public Dictionary<string , float>  weighting = new Dictionary<string, float>() { {"A",0.7f},{"B",0.5f},{"C",0.3f},{"D",0.15f} };
	float[]  counterArray= new float[]{0.0f,0.0f,0.0f,0.0f,0.0f}; 
	List<KeyValuePair<int, float>> PercentageList = new List<KeyValuePair<int, float>>();

	void Start()
	{
		PercentageList.Add(new KeyValuePair<int, float>(4,.15f));
		PercentageList.Add(new KeyValuePair<int, float>(3,.30f));
		PercentageList.Add(new KeyValuePair<int, float>(2,.50f));
		PercentageList.Add(new KeyValuePair<int, float>(1,.50f));
		PercentageList.Add(new KeyValuePair<int, float>(0,.70f));
	}
	/*
	public string ChooseByRandom()
	{
		int _index = Random.Range(0,counterArray.Length);
		counterArray[_index]=counterArray[_index] +1;
		
		while(true)
		{
			_index = Random.Range(0,counterArray.Length);
			Debug.Log("index="+counterArray[_index] +"divider = "+(float)(counterArray[_index] * 0.100 )+" dictionary = "+ weighting[_index].value );
			//if( (counterArray[_index] / 100 ) < weighting[_index].value )
					return weighting.Keys.ElementAt(_index);
			
		}
	}
	*/
	
	void resetCounterArray()
	{
		for(int i=0; i< counterArray.Length ; i++)
		{
			counterArray[i]= 0.0f;	
		}
	}
	
	void checkCounterArray()   // check whether counterArray is need to be reset by checking the individual value is equal to the percentageList
	{
		bool isReset = false;
		for(int i=0; i< counterArray.Length ; i++)
		{
			if(counterArray[i] >= PercentageList[i].Value )	{
				isReset = true;				
			}
			else
				isReset = false;
		}
		if(isReset)
			resetCounterArray();
	}
	
	public int ChooseByRandom()
	{
		try
		{
			checkCounterArray();  
			float f_index = 0.0f;
			int _index = 0;
			
			while(true)    //Choose counterArray index randomly
			{ 
				f_index = UnityEngine.Random.Range(0.0f, 5.0f);
				_index = (int)f_index;
				if((counterArray[_index] <= PercentageList[_index].Value)){
					counterArray[_index]=counterArray[_index] + 0.05f;	
					return PercentageList[_index].Key;	
				}
			}
		}
		catch(InvalidCastException e)
		{
			Debug.Log(e);
		}
		return 100;	
	}
	
	
}
