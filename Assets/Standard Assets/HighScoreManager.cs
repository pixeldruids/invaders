using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class HighScoreManager : MonoBehaviour {

	//static HighScoreManager instance;
	List<HighScoreTable> _highscoretable;
	int noOfScoreToStore = 10;
	//bool isDelete = true;  // uncomment when enable delete button
	
	void Awake()
	{
		// Make this manager script be a singleton
	//	instance = this;
		DontDestroyOnLoad (this);
	}
	
	
	// Use this for initialization
	void Start () 
	{
		_highscoretable = new List<HighScoreTable>();
		_highscoretable = GetHighScoreTable();
		
		if(_highscoretable.Count == 0){
			InitializePlayerPrefs();	
		}
		
	//	foreach(HighScoreTable scoress in _highscoretable)
	//		Debug.Log("scoress = "+scoress.score+"  name ="+scoress.playerName1+"  mode ="+scoress.mode+"  player2 ="+scoress.playerName2+" score2 ="+scoress.score2);
	}	
	
	void InitializePlayerPrefs()
	{
		for(int i= noOfScoreToStore - _highscoretable.Count; i<noOfScoreToStore; i++){
			_highscoretable.Add(InsertIntoHighScoreTable ("","", 1 ,1,1)); 
		}
	}
	
	

	// Update is called once per frame
	void Update () {
		// uncomment to enable delete button so than you can delete previous stored data
	//	if(isDelete)
	//		SaveHighScores ("India", (int)Random.Range (1, 10000),(int)Random.Range(2,11),Random.Range(0,5.0f));     //for testing
		
		 if (Input.GetKeyDown(KeyCode.Escape))
     	{
			Application.Quit();
		}
	}
	
/*	public int GetHighscore(int _mode)
	{
		foreach(HighScoreTable scoress in _highscoretable){
			if(_mode == scoress.mode==2){
				GameManager.highscore = scoress.score;
				GameManager.highscore2 = scoress.score2;
				break;
			}
			else{
				GameManager.highscore = scoress.score;
				break;
			}
			
		}
	}
	*/
	public List<HighScoreTable> GetHighScores()   // return the highscore in List format 
	{
		_highscoretable = GetHighScoreTable();
		return _highscoretable;
	}
	
	List<HighScoreTable> InitializePlayerPrefs(List<HighScoreTable> _temp)
	{
		for(int i= noOfScoreToStore - _temp.Count; i< noOfScoreToStore; i++){
			_temp.Add(InsertIntoHighScoreTable ("","", 0 ,0,0)); 
		}
		return _temp;
	}

	
	List<HighScoreTable> GetHighScoreTable()
	{
		int i=1;
		List<HighScoreTable> _scores = new List<HighScoreTable>();
		
		while(i<noOfScoreToStore && PlayerPrefs.HasKey(i+"score"))
		{
				HighScoreTable tempTable = new HighScoreTable();
				tempTable.playerName1 = PlayerPrefs.GetString(i+"playerName1");
				tempTable.playerName2 = PlayerPrefs.GetString(i+"playerName2");
				tempTable.score = PlayerPrefs.GetInt(i+"score");
				tempTable.score2 = PlayerPrefs.GetInt(i+"score2");
				tempTable.mode = PlayerPrefs.GetInt(i+"mode");
				_scores.Add(tempTable);
				i++;
			
		}
		_scores = InitializePlayerPrefs(_scores);
		return _scores;
	}
	
	HighScoreTable InsertIntoHighScoreTable(string _playerName1,string _playerName2 , int _Score,int _Score2, int _mode)
	{
		HighScoreTable tempTable = new HighScoreTable();
		tempTable.playerName1 = _playerName1;
		tempTable.playerName2 = _playerName2;
		tempTable.score = _Score;
		tempTable.score2 = _Score2;
		tempTable.mode = _mode;
		return tempTable;
	}
	
	
public void SaveHighScores(string _playerName1,string _playerName2, int _currentScore, int _currentScore2,int _mode)
	{
		int i = 1;
		List<HighScoreTable> _scores = new List<HighScoreTable>();
		
		_scores = GetHighScoreTable();
		
		if(_scores.Count ==0) // if there is no previous record 
		{
			HighScoreTable tempTable =InsertIntoHighScoreTable(_playerName1,_playerName2,_currentScore,_currentScore2,_mode);
			_scores.Add(tempTable);
		}	
		else  // if previous record exists
		{ 
			for(i=1;i<=_scores.Count && i<noOfScoreToStore ; i++)
			{
				 if(_mode != _scores [i - 1].mode){      // if gameplay mode is not equal then add the score 
				 	HighScoreTable tempTable =  InsertIntoHighScoreTable(_playerName1,_playerName2,_currentScore,_currentScore2,_mode);
              		_scores.Insert (i - 1, tempTable);
              		break;
				 }
				 else if(_mode == _scores [i - 1].mode)  // if gameplay mode is equal then compare the scores
				 {
					 if (_currentScore > _scores [i - 1].score)    // current score is high among previous score  
					 {
	              		HighScoreTable tempTable =  InsertIntoHighScoreTable(_playerName1,_playerName2,_currentScore,_currentScore2,_mode);
	              		_scores.Insert (i - 1, tempTable);
	              		break;
	         		 }
	          		 else if (_currentScore == _scores [i - 1].score)    // equal scores then compare player names
	          		 {	
	          		 	if(_playerName1 != _scores[i-1].playerName1)
	          		 	{	
	          		 		HighScoreTable tempTable = InsertIntoHighScoreTable(_playerName1,_playerName2,_currentScore,_currentScore2,_mode);
	              			_scores.Insert (i - 1, tempTable);
	              			break;
	          		 	}	
	          		 	          		 	
	          		} // end of else in (equal scores then compare Player names)
				 }			
			}// end of for loop
			
		}// end of else in (if previous record exists)
		
		i = 1;
      		while (i<=noOfScoreToStore && i<=_scores.Count) 
      		{
         		PlayerPrefs.SetString ( i +"playerName1", _scores [i - 1].playerName1);
         		PlayerPrefs.SetString ( i +"playerName2", _scores [i - 1].playerName2);
         		PlayerPrefs.SetInt ( i +"score", _scores[i - 1].score);
         		PlayerPrefs.SetInt ( i +"score2", _scores[i - 1].score2);
         		PlayerPrefs.SetInt (i+"mode",_scores[i-1].mode);
         		i++;
       		}
	_highscoretable = GetHighScoreTable();
	}
		
}


public class HighScoreTable
{
	public int mode;
	public string playerName1;
	public string playerName2;
	public int score;
	public int score2;
	
}