using UnityEngine;
using System.Collections;

public class Utility : MonoBehaviour {
	
	public static int score;
	public static int score1;
	public static int LoadVolume()
	{
		int volume = PlayerPrefs.GetInt("Volume",1);
		if(volume == 1)	
		{			
			AudioListener.volume = 1.0f;
		}
		else
		{
			AudioListener.volume = 0.0f;
		}	
		return volume;
	}
	
	public static void saveVolume(int vol)
	{
		PlayerPrefs.SetInt("Volume",vol);
		AudioListener.volume = vol;
	}
	
	public static string getFormatedNumber(int number)
	{
		string tempStr = number.ToString("00000000");
		return tempStr;
	}
	
	public static void SetVolumeForPauseScreen(int vol)
	{
		if(LoadVolume()==1)
			AudioListener.volume = vol;
		else
			AudioListener.volume = 0;
	}
}
