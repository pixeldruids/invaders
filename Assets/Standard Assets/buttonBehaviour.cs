using UnityEngine;
using System.Collections;

public class buttonBehaviour : MonoBehaviour {
	
	public Texture selectedTexture;
	public Texture notSelectedTexture;
	
	
	// Use this for initialization
	void Start () {	
		setNotSelected();
	}
	
	public void setSelected()
	{
		updateTexture(selectedTexture);
	}
	public void setNotSelected()
	{
		updateTexture(notSelectedTexture);
	}
	void updateTexture(Texture tex)
	{
		renderer.material.SetTexture("_MainTex",tex);
	}

}
