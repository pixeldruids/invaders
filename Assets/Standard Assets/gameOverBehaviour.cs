using UnityEngine;
using System.Collections;

public class gameOverBehaviour : MonoBehaviour {

	public TextMesh p1Line;
	public TextMesh p2Line;
	public TextMesh p1Score;
	public TextMesh p2Score;
	int currentSelected = 0;
	
	public buttonBehaviour []ButtonList;	
	
	public AudioClip menuSelectionSound;
	public AudioClip menuEnterSound;
	string levelName = "";
	bool isFirstTime;
	bool startListeningInput;
	
	// Use this for initialization
	void Start () 
	{
		isFirstTime = true;
		iTween.MoveTo(gameObject, iTween.Hash("x", 0, "y", 0, "easeType", "easeInOutExpo", "delay", .1));	
		currentSelected = 0;
		InputManager.Instance.greenThrottle.Reset();
		startListeningInput = false;
		for(int i = 0; i < ButtonList.Length; i++)
		{
			iTween.MoveTo(ButtonList[i].gameObject,iTween.Hash("x",27.6,"time",0.25f,"delay",3.0f));
		}
		Invoke ("enableInput",3.25f);
		if(InputManager.gameMode == 0)
		{
			adjustTransforms(18.9f);	
			p1Score.text = Utility.score.ToString("00000000");
			p1Score.renderer.material.color = Color.yellow;			
			p1Line.renderer.material.color = Color.yellow;
			p2Score.renderer.enabled = false;
			p2Line.renderer.enabled = false;

		}
		if(InputManager.gameMode == 1)
		{
			adjustTransforms(-5.2f);
			p1Score.text = Utility.score.ToString("00000000");
			p2Score.text = Utility.score1.ToString("00000000");
			if(Utility.score > Utility.score1)
			{
				p1Score.renderer.material.color = Color.yellow;
				p1Line.renderer.material.color = Color.yellow;
				p2Score.renderer.material.color = Color.white;
				p2Line.renderer.material.color = Color.white;
			}
			else if(Utility.score < Utility.score1)
			{
				p1Score.renderer.material.color = Color.white;
				p1Line.renderer.material.color = Color.white;
				p2Score.renderer.material.color = Color.yellow;
				p2Line.renderer.material.color = Color.yellow;
	
			}
			else
			{
				p1Score.renderer.material.color = Color.yellow;
				p1Line.renderer.material.color = Color.yellow;
				p2Score.renderer.material.color = Color.yellow;
				p2Line.renderer.material.color = Color.yellow;
			}
		}
		
		Invoke("MakeSomeDelayOnUpdate",1.5f);
		
	}
	void enableInput()
	{
		startListeningInput = true;
	}
	void adjustTransforms(float xVal)
	{
			Vector3 localPos = p1Score.transform.localPosition;
			localPos.x = xVal;
			p1Score.transform.localPosition = localPos;
			localPos = p1Line.transform.localPosition;
			localPos.x = xVal;
			p1Line.transform.localPosition = localPos;

	}
	
	void PlayMenuSelectionSound()
	{
		audio.clip = menuSelectionSound;
		audio.Play();
	}
	
	void PlayMenuEnterSound()
	{
		audio.clip = menuEnterSound;
		audio.Play();
	}
	
	
	void MakeSomeDelayOnUpdate()
	{
		isFirstTime = false;
	}
	
	
	// Update is called once per frame
	void Update () {
		
		if(Input.GetKeyDown(KeyCode.Escape) || InputManager.Instance.GetKey(InputManager.IB_SELECT))
		{
			Application.LoadLevel("Menu");
		}
		
		if(isFirstTime)	return;
		if(!startListeningInput) return;
		for(int i = 0; i < ButtonList.Length; i++)
		{
			if(currentSelected == i) ButtonList[i].setSelected();
			else ButtonList[i].setNotSelected();
		}

		if(Input.GetKeyDown(KeyCode.UpArrow) || InputManager.Instance.GetKeyDown(InputManager.IB_FORWARD))
		{
			--currentSelected;
			PlayMenuSelectionSound();
		}
		if(Input.GetKeyDown(KeyCode.DownArrow) || InputManager.Instance.GetKeyDown(InputManager.IB_BACK))
		{
			++currentSelected;
			PlayMenuSelectionSound();
		}
		if(currentSelected > 1) currentSelected = 0;
		if(currentSelected < 0) currentSelected = 1;
		if(Input.GetKeyDown(KeyCode.Space) || InputManager.Instance.GetKey(InputManager.IB_FIRE))
		{			
			switch(currentSelected)
			{
			case 0:			
				PlayMenuEnterSound();	
				levelName ="MainGame";
				break;
			case 1:				
				PlayMenuEnterSound();
				levelName ="Menu";
				break;
			}
			Invoke("LoadLevel",0.2f);
		}			
	}
	
	void LoadLevel()
	{
		Application.LoadLevel(levelName);
	}
	
}
