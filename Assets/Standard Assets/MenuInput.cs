using UnityEngine;
using System.Collections;

public class MenuInput : MonoBehaviour {
	
	int currentSelected;
	public buttonBehaviour []ButtonList;	
	public AudioClip menuSelectionSound;
	public AudioClip menuEnterSound;
	public AudioClip menuBackSound;
	
	string levelName = "";
	
	//ArrayList PositionList = new ArrayList();
	//GameObject selector;
	// Use this for initialization
	void Start () 
	{
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		currentSelected = 0;		
		InputManager.Instance.greenThrottle.Reset();		
		Utility.LoadVolume();	
		(GameObject.FindWithTag("menubgmusic").GetComponent("AudioSource") as AudioSource).enabled = true;
	}
	
	void PlayMenuBackSound()
	{
		audio.clip = menuBackSound;
		audio.Play();
	} 
	
	void PlayMenuSelectionSound()
	{
		audio.clip = menuSelectionSound;
		audio.Play();
	}
	
	void PlayMenuEnterSound()
	{
		audio.clip = menuEnterSound;
		audio.Play();
	}
	
	// Update is called once per frame
	void Update () 
	{
		for(int i = 0; i < ButtonList.Length; i++)
		{
			if(currentSelected == i) ButtonList[i].setSelected();
			else ButtonList[i].setNotSelected();
		}
		if(Input.GetKeyDown(KeyCode.UpArrow) || InputManager.Instance.GetKeyDown(InputManager.IB_FORWARD))
		{
			PlayMenuSelectionSound();	
			--currentSelected;
		}
		if(Input.GetKeyDown(KeyCode.DownArrow) || InputManager.Instance.GetKeyDown(InputManager.IB_BACK))
		{
			PlayMenuSelectionSound();	
			++currentSelected;
		}
		if(currentSelected > 3) currentSelected = 0;
		if(currentSelected < 0) currentSelected = 3;
		
		if(Input.GetKeyDown(KeyCode.Space) || InputManager.Instance.GetKeyDown(InputManager.IB_FIRE))
		{			
			switch(currentSelected)
			{
			case 0:
				InputManager.gameMode = 0;
				PlayMenuEnterSound();
				levelName ="MainGame"; 
				Invoke("LoadLevel",0.2f);
				break;
			case 1:
				PlayMenuEnterSound();
				InputManager.gameMode = 1;
				levelName ="MainGame";
				Invoke("LoadLevel",0.2f);
				break;
			case 2:
				PlayMenuEnterSound();
				levelName ="HowToPlay";
				Invoke("LoadLevel",0.2f);
				break;
			case 3:
				PlayMenuEnterSound();
				levelName ="Settings";
				Invoke("LoadLevel",0.2f);
				break;
			}
		}				
		if(Input.GetKeyDown(KeyCode.Escape) || InputManager.Instance.GetKey(InputManager.IB_SELECT))
		{			
			PlayMenuBackSound();
			Application.Quit();
		}				

	}	
	
	
	void LoadLevel()
	{
		Application.LoadLevel(levelName);
	}
	
}
