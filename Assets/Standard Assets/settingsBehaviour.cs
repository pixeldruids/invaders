using UnityEngine;
using System.Collections;

public class settingsBehaviour : MonoBehaviour
{
	public buttonBehaviour []ButtonList;	
	public buttonBehaviour []ParentButtonList;
	public AudioClip menuSelectionSound;
	public AudioClip menuEnterSound;
	public AudioClip menuBackSound;
	int parentSelection;
	int currentSelection;
	
	// Use this for initialization
	void Start () {
		InputManager.Instance.greenThrottle.Reset();
		int vol = Utility.LoadVolume();
		if(vol == 0) currentSelection = 0;
		else currentSelection = 1;
		parentSelection = 0;
	}
	
	void PlayMenuSelectionSound()
	{
		audio.clip = menuSelectionSound;
		audio.Play();
	}
	
	void PlayMenuEnterSound()
	{
		audio.clip = menuEnterSound;
		audio.Play();
	}
	
	void PlayMenuBackSound()
	{
		audio.clip = menuBackSound;
		audio.Play();
	} 
	
	// Update is called once per frame
	void Update () {
		
		if(Input.GetKeyDown(KeyCode.Escape) || InputManager.Instance.GetKey(InputManager.IB_SELECT))
		{			
			PlayMenuBackSound();
			Application.Quit();
		}	
		
		if(parentSelection == 0)
			ChangeSoundStatus();
		else
			LoadCreditScene();
		
		if(Input.GetKeyDown(KeyCode.UpArrow) || InputManager.Instance.GetKeyDown(InputManager.IB_FORWARD))
		{
			PlayMenuSelectionSound();	
			--parentSelection;
		}
		if(Input.GetKeyDown(KeyCode.DownArrow) || InputManager.Instance.GetKeyDown(InputManager.IB_BACK))
		{
			PlayMenuSelectionSound();	
			++parentSelection;
		}
		
		if(parentSelection > 1) parentSelection = 0;
		if(parentSelection < 0) parentSelection = 1;
		
		
		if(Input.GetKeyDown(KeyCode.Escape) || InputManager.Instance.GetKeyDown(InputManager.IB_PREV))
		{
			PlayMenuBackSound();
			Application.LoadLevel("Menu");
		}
		
	
		for(int i = 0; i < ButtonList.Length; i++)
		{
			if(currentSelection == i) 
				ButtonList[i].setSelected();
			else 
				ButtonList[i].setNotSelected();
		}
		
		for(int i = 0; i < ParentButtonList.Length; i++)
		{
			if(parentSelection == i) 
				ParentButtonList[i].setSelected();
			else 
				ParentButtonList[i].setNotSelected();
		}
	}
	
	
	void ChangeSoundStatus()
	{
		if(Input.GetKeyDown(KeyCode.LeftArrow) || InputManager.Instance.GetKeyDown(InputManager.IB_LEFT))
		{
			--currentSelection;
			PlayMenuSelectionSound();
		}
		if(Input.GetKeyDown(KeyCode.RightArrow) || InputManager.Instance.GetKeyDown(InputManager.IB_RIGHT))
		{
			++currentSelection;
			PlayMenuSelectionSound();
		}
		
		if(currentSelection > 1) currentSelection = 0;
		if(currentSelection < 0) currentSelection = 1;
		
		if(currentSelection == 0) Utility.saveVolume(0);    // Sound off
		if(currentSelection == 1) Utility.saveVolume(1);    // Sound on
	}
	
	void LoadCreditScene()
	{
		if(Input.GetKeyDown(KeyCode.Space) || InputManager.Instance.GetKey(InputManager.IB_FIRE))
		{
			PlayMenuEnterSound();
			Invoke("Credits",0.2f);
		}
	}
	
	void Credits()
	{
		Application.LoadLevel("Credits");
	}
}
