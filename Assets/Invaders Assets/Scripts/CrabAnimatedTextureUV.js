
 var uvAnimationTileX = 24; //Here you can place the number of columns of your sheet. 
                           //The above sheet has 24

 var uvAnimationTileY = 1; //Here you can place the number of rows of your sheet. 
                          //The above sheet has 1
var framesPerSecond = 10.0;

var playOnce : boolean = false;

var isPlayOnceFinished : boolean = false;

 var index : int  = 0;

var size : Vector2 ;
   	
var offset :Vector2 ;
 
 
function Start()
{
	InvokeRepeating("updateTiming",0.0f,0.12f);
} 
 
function updateTiming()
{
	index++;
}
function changeAnimation(tileX:int,tileY:int, fps:int, oneplay:boolean,_dindex:int)
{
	uvAnimationTileX = tileX;
	uvAnimationTileY = tileY;
	framesPerSecond = fps;
	playOnce =oneplay;
	index = _dindex;
	isPlayOnceFinished = false;
}

function Update () 
{   
    // repeat when exhausting all frames
    index = index % (uvAnimationTileX * uvAnimationTileY);
 
    // Size of every tile
    size = Vector2 (1.0 / uvAnimationTileX, 1.0 / uvAnimationTileY);

    if(playOnce)
    {
    	if(!isPlayOnceFinished)
    	{
    		// split into horizontal and vertical index
    		var uIndex = index % uvAnimationTileX;
    		var vIndex = index / uvAnimationTileX;
    		 if(uIndex == (uvAnimationTileX-1)){
    		 	uIndex = 0;
    		 	isPlayOnceFinished = true;
    		 	}
    	}
    	else
    	{
    		//Debug.Log(" finished animation");
    	}
    }
    else
    {
    	// split into horizontal and vertical index
    	 uIndex = index % uvAnimationTileX;
    	 vIndex = index / uvAnimationTileX;	
    }
  
    offset = Vector2 (uIndex * size.x, 1.0 - size.y - vIndex * size.y);   
    
    renderer.material.SetTextureOffset ("_MainTex", offset);
    renderer.material.SetTextureScale ("_MainTex", size);
}