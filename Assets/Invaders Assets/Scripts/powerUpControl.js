#pragma strict
public enum powerUpTypes
{
	BUBBLE  ,   //0
	FLASH,    //1
	REGROWTH, //2
	CRABAGEDDON, //3 
	SHARK, //4 
	NONE //5
}


public static var currentPowerUp  = powerUpTypes.NONE;

public static var prevPowerUp  = powerUpTypes.NONE;

static var powerUpCount : int;

private var FreezeTimer : int = 0;

var isFreezeTimer : boolean = false;

var startTimer : int = 0;

var invaderCtrl:InvaderControl;

static var powerUpType_String : String; 

var powerUpCamera : Camera;
var powerUpCameraClone : Camera;

var crabBubble : GameObject;   
var crabBubbleClone : Transform;

var CurrentPlayer :String;

var sharkObject : GameObject;


function Start () 
{
 	powerUpCount = System.Enum.GetValues(powerUpTypes).length;
	invaderCtrl =GameObject.Find("_Manager").GetComponent("InvaderControl")as InvaderControl;
	
}


function DeactivePowerUp()
{
	startTimer = 0.0f ; 
	
	switch(prevPowerUp)
	{
		case powerUpTypes.BUBBLE:
								DeActivateBubbleShield();
								break;
		case powerUpTypes.CRABAGEDDON:
								DeActivateCrabageddon();
								break;	
		case powerUpTypes.NONE:
								break;																														
	}
	
}



function DestroyPowerUp()
{
	startTimer = 0.0f ; 
	
	switch(prevPowerUp)
	{
		case powerUpTypes.SHARK:
								DeActiveSharkPowerUp();
								break; 
		case powerUpTypes.BUBBLE:
								DeActivateBubbleShield();
								break;
		case powerUpTypes.FLASH:
								DeActivateInvadersFreeze();
								break;
		case powerUpTypes.CRABAGEDDON:
								DeActivateCrabageddon();
								break;	
		case powerUpTypes.NONE:
								break;																														
	}
	
}


function DeActiveSharkPowerUp()
{
	if(GameObject.FindWithTag("sharkpowerup"))
		Destroy( GameObject.FindWithTag("sharkpowerup") );
}

function DeActivateBubbleShield()
{
	if(crabBubbleClone)
		Destroy(crabBubbleClone.gameObject);
}


function DeActivateInvadersFreeze()
{
	startTimer = Time.time ;
}

function DeActivateCrabageddon()
{
	if(GameObject.Find("player1"))
	{
		if(GameObject.Find("player1").GetComponent(CrabageddonPowerUp))
		{
			var crabageddonPowerUpScript : CrabageddonPowerUp = (GameObject.Find("player1").GetComponent("CrabageddonPowerUp")) as CrabageddonPowerUp;
			crabageddonPowerUpScript.StopCrabageddonPowerUp();	
		}
	}				

	if(InputManager.gameMode!=0)
	{
		if(GameObject.Find("player2"))
		{
			if(GameObject.Find("player2").GetComponent(CrabageddonPowerUp))
			{
				var crabageddonPowerUpScript2 : CrabageddonPowerUp = (GameObject.Find("player2").GetComponent("CrabageddonPowerUp")) as CrabageddonPowerUp;
				crabageddonPowerUpScript2.StopCrabageddonPowerUp();	
			}
			
		}	
	}
}

function Update () 
{
	
	switch(currentPowerUp)
	{
		case powerUpTypes.SHARK:
								ActiveSharkPowerUp();
								break;
		case powerUpTypes.BUBBLE:
								ActivateBubbleShield();
								break;
		case powerUpTypes.REGROWTH:
								RegrowthProtectiveShields();
								break;
		case powerUpTypes.FLASH:
								ActivateInvadersFreeze();
								break;
		case powerUpTypes.CRABAGEDDON:
								ActivateCrabageddon();
								break;	
		case powerUpTypes.NONE:
								break;																														
	}
}



function ActiveSharkPowerUp()
{
	currentPowerUp = powerUpTypes.NONE;
	invaderCtrl.updateInvadersStatus();
	
	var randomsGem : RandomGenerator = gameObject.GetComponent("RandomGenerator")as RandomGenerator;
	var _row :int = randomsGem.ChooseByRandom();
	yield;
	var sharkClone : GameObject = Instantiate(sharkObject,Vector3(1000.0,1000.0,1000.0),Quaternion.identity);
	sharkClone.gameObject.name = "SharkPowerUp";
	var sharkBehaviour : SharkPowerUpBehaviour = sharkClone.GetComponent("SharkPowerUpBehaviour") as SharkPowerUpBehaviour;
	yield WaitForSeconds(2);
	while( (invaderCtrl.GetInvaderYPosValue(_row)==1000 ) )
	{
		invaderCtrl.updateInvadersStatus();
		_row = randomsGem.ChooseByRandom();
		yield;
	}
	sharkBehaviour.ReadInvaderRow(_row);
	sharkBehaviour.Makeshark();
}


function GetCurrentPlayer():String
{
	return CurrentPlayer;
}

function SetCurrentPlayer(_type :String)
{
	CurrentPlayer = _type;
}

function LateUpdate()
{
	if(isFreezeTimer)
	{
		if((startTimer - Time.time ) <= 0)
		{
			invaderCtrl.SetInvaderFreeze(false);
			isFreezeTimer = false;
			invaderCtrl.TurnInvaderColors(Color.white);
			invaderCtrl.TurnInvaderAinmation(true);
		}
	}
	else
	{
		invaderCtrl.SetInvaderFreeze(false);
		invaderCtrl.TurnInvaderColors(Color.white);
		invaderCtrl.TurnInvaderAinmation(true);
	}
}

function ActivateCrabageddon()
{
	var _pCtrl : PlayerControl;
	currentPowerUp = powerUpTypes.NONE;
	startTimer = Time.time + 10;
	
	if(CurrentPlayer == "player1")
	{
		_pCtrl = GameObject.Find("player1").GetComponent("PlayerControl") as PlayerControl;
		GameObject.Find("player1").AddComponent("CrabageddonPowerUp");
		var crabageddonPowerUpScript : CrabageddonPowerUp = GameObject.Find("player1").GetComponent("CrabageddonPowerUp") as CrabageddonPowerUp;
		yield WaitForSeconds(0.5);
		crabageddonPowerUpScript.RepeatCrabageddonFor10Sec(startTimer);
		_pCtrl.isCrabbageddon = true;
	}
	else
	{
		_pCtrl = GameObject.Find("player2").GetComponent("PlayerControl") as PlayerControl;
		GameObject.Find("player2").AddComponent("CrabageddonPowerUp");
		crabageddonPowerUpScript  = GameObject.Find("player2").GetComponent("CrabageddonPowerUp") as CrabageddonPowerUp;
		yield WaitForSeconds(0.5);
		crabageddonPowerUpScript.RepeatCrabageddonFor10Sec(startTimer);
		_pCtrl.isCrabbageddon = true;
	}
	
}


function ActivateBubbleShield()
{
	if(GameObject.FindWithTag("bubble")) return;
	 crabBubbleClone = Instantiate (crabBubble.transform); 
	 currentPowerUp = powerUpTypes.NONE;	 
}

static function ConvertToEnum(_types:int) :String
{
	var temp : powerUpTypes  = _types;
	powerUpType_String = temp.ToString();
	return temp.ToString();
}

function ActivateInvadersFreeze()
{
	invaderCtrl.SetInvaderFreeze(true);
	currentPowerUp = powerUpTypes.NONE;
	isFreezeTimer = true;
	//InputManager.isInvadersfreeze = true;	
	startTimer = Time.time+9;
	var  nColor :Color = Color(0,1,1,1);
	invaderCtrl.TurnInvaderColors(nColor);
	invaderCtrl.TurnInvaderAinmation(false);
}


function RegrowthProtectiveShields()
{
	invaderCtrl.RegrowthBunkers();	
	currentPowerUp = powerUpTypes.NONE;
}


static function StringToEnum(_powerName : String,_playerName:String)
{
	var _powerUpCtrl :powerUpControl = GameObject.Find("_Manager").GetComponent("powerUpControl") as powerUpControl;
	_powerUpCtrl.SetCurrentPlayer(_playerName);
	var temp : powerUpTypes = powerUpTypes.NONE;
	temp = System.Enum.Parse( typeof( temp ), _powerName );
	currentPowerUp = temp;
	prevPowerUp = temp;
}