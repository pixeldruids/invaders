// Common functions for bunkers

var hole : Texture2D;
private var holeTexArray : Color[];
var explosion : GameObject;
static var use : BunkerControl;

function Awake () {
	use = this;
	holeTexArray = hole.GetPixels();	
}

function Explode (pos : Vector3) {
	Instantiate(explosion, pos, Quaternion.identity);	
}

// Unity has no real 2D texture collision detection, so we sort of fake it by using raycasting
// We'll check a given pixel in a texture, and if it's completely clear, then we count it as a miss; otherwise we call it a hit
function CheckAlpha (tex : Texture2D, pixelUV : Vector2) : boolean {
	var uvX : int = pixelUV.x * tex.width;
	var uvY : int = pixelUV.y * tex.height;

	// If the hit point isn't transparent, then we hit something, so copy the hole texture alpha to an area around that point
	if (tex.GetPixel(uvX, uvY).a != 0.0) {
		var startX = uvX;
		// Half the time, overlay the hole alpha upside-down & backwards just for variety
		if (Random.value > .5) {
			var dir = 1;
			startX -= hole.width/2;
			uvY -= hole.height/2;
		}
		else {
			dir = -1;
			startX += hole.width/2;
			uvY += hole.height/2;
		}
		// Overlay hole texture alpha onto bunker texture around the hit point
		// GetPixel/SetPixel is automatically clamped if the texture is set to clamp, so we don't have to worry about going out of bounds
		for (var y = 0; y < hole.height; y++) {
			uvX = startX;
			for (var x = 0; x < hole.width; x++) {
				var thisPix = tex.GetPixel(uvX, uvY);
				thisPix.a *= holeTexArray[x + y*hole.width].a;
				tex.SetPixel(uvX, uvY, thisPix);
				uvX += dir;
			}
			uvY += dir;
		}

		tex.Apply();

		return true;
	}
	// Otherwise, the hit point was transparent, so we didn't actually hit anything
	else {
		return false;
	}
}