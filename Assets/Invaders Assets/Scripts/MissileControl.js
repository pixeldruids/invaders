// Controls what happens when a missile hits an object

var smallSparks : GameObject;
var smallSparks_Alien : GameObject;

function OnCollisionEnter (other : Collision) 
{

	if (other.gameObject.tag == "ufo") {
		InvaderControl.use.DestroyUFO (other.gameObject, this.name.ToString());
		Destroy (gameObject);
		return;
	}
	else if (other.gameObject.tag == "powerupchest") {
		InvaderControl.use.DestroyPowerUpChest (other.gameObject, other.gameObject.tag.ToString(),other.collider.transform.position,gameObject.name);
	}
	else if (other.gameObject.tag == "alien") {
		Destroy (other.gameObject);
		InvaderControl.use.InvaderDestroyed (parseInt(other.gameObject.name), other.collider.transform.position, this.name.ToString(),false);
	}
	else if(other.gameObject.name == "bomb(Clone)"){
		
	}
	if(other.gameObject.tag.ToLower().Contains("bubble"))
	{
		return;
	}
	if(other.gameObject.name == gameObject.name) return;
	
	if(this.name.Equals("player2missile"))Explode(2,other.gameObject.tag);
	if(this.name.Equals("player1missile"))Explode(1,other.gameObject.tag);
	
}
// This function is also called from BunkerBehavior, which is why it's not part of OnCollisionEnter
function Explode(playerNo : int,_name:String) {
	
 	if( (_name == "alien") || (_name == "ceilling"))
		Instantiate (smallSparks_Alien, transform.position - Vector3.forward*2.0, Quaternion.identity);	
	else if(_name != "powerupchest")
		Instantiate (smallSparks, transform.position - Vector3.forward*2.0, Quaternion.identity);
		
	Destroy (gameObject);
}


