// Blinks text on and off

var interval = .5;
var textAnchor : Transform;

function Start() {
	// Position the text from a 3D object so it's in the same relative spot on 4:3 or widescreen displays
	transform.position.x = Camera.main.WorldToViewportPoint(textAnchor.position).x;

	while (true) {
		guiText.enabled = !guiText.enabled;
		yield WaitForSeconds(interval);
	}
}


