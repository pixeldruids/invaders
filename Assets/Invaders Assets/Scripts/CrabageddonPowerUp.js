/*   
	CRABIDLE_L ,
	CRABIDLE_R ,
	CRABWALK_L ,
	CRABWALK_R ,
	CRABSHOOT_L ,
	CRABSHOOT_R
*/

var playerTextureManager : PlayerTextureManager;
var _pCtrl : PlayerControl;

var CurrentPlayer :String;

private var grayScaleTextureArray:Texture2D[] ;
var colorsArray : Color[];
private var arraySize :int = 6;
var arrayIndex :int;

function Awake()
{
	grayScaleTextureArray = new Texture2D[arraySize];	
	
	for (var i=0 ; i< arraySize ; i++)
	{
		grayScaleTextureArray[i] = Resources.Load("GrayScale/crab"+(i+1));
	}
	arrayIndex = 0;
	AssignColorArray();
}


function Start()
{
	playerTextureManager = gameObject.GetComponent("PlayerTextureManager") as PlayerTextureManager;
	_pCtrl = gameObject.GetComponent("PlayerControl") as PlayerControl;
	
	if(!playerTextureManager)
		Debug.Log("nil textre mgr");
	if(!_pCtrl)
		Debug.Log("pctrl is nil");	
}

function AssignColorArray()
{
	colorsArray = new Color[21];
	colorsArray[0] = ConvertColor(148,0,211,255);
	colorsArray[1] = ConvertColor(0,128,0,255);
	colorsArray[2] = ConvertColor(173,255,47,255);
	colorsArray[3] = ConvertColor(165,42,42,255);
	colorsArray[4] = ConvertColor(139,0,0,255);
	colorsArray[5] = ConvertColor(255,0,0,255);
	colorsArray[6] = ConvertColor(0,128,128,255);
	colorsArray[7] = ConvertColor(70,130,180,255);
	colorsArray[8] = ConvertColor(205,92,92,255);
	colorsArray[9] = ConvertColor(186,85,211,255);
	colorsArray[10] = ConvertColor(124,252,0,255);
	colorsArray[11] = ConvertColor(0,139,139,255);
	colorsArray[12] = ConvertColor(240,128,128,255);
	colorsArray[13] = ConvertColor(32,178,170,255);
	colorsArray[14] = ConvertColor(154,208,50,255);
	colorsArray[15] = ConvertColor(255,165,0,255);
	colorsArray[16] = ConvertColor(72,209,204,255);
	colorsArray[17] = ConvertColor(72,61,139,255);
	colorsArray[18] = ConvertColor(255,61,139,255);
	colorsArray[19] = ConvertColor(139,120,11,255);
	colorsArray[20] = ConvertColor(255,15,200,255);

}

function ConvertColor (r : int, g : int, b : int, a : int) : Color {
    return Color(r/255.0, g/255.0, b/255.0, a/255.0);
}

function CheckAnyMissileAlive(_pCtrl : PlayerControl)
{
/*	while(1)
	{
		yield;
		if(GameObject.Find("player1missile"))
			_pCtrl.SetMissileStatus(true,"player1");		
		else if(GameObject.Find("player2missile"))
			_pCtrl.SetMissileStatus(true,"player2");		
		else			
			return;	
	}*/
	
}

function TurnCrabColorChange(_status:boolean)
{
	if(_status) // turn crab color
	{
		arrayIndex++;
		if(arrayIndex>19)
			arrayIndex = 0;
		var  nColor = colorsArray[arrayIndex];
		playerTextureManager.TurnCrabColors(nColor);
	}
	else  // turn crab normal state
	{
		playerTextureManager.TurnCrabColors(Color.white);
	}
}

//increase the crab moving speed
function TurnCrabSpeed(_status : boolean) 
{
	_pCtrl.SetplayerMoveSpeed(_status);
}

function changeColor()
{
	TurnCrabColorChange(true);
}
function RepeatCrabageddonFor10Sec(startTimer:int)
{
	TurnCrabSpeed(true);
	playerTextureManager.AssignCrabTextureArray(grayScaleTextureArray);
	InvokeRepeating("changeColor",0.0f,0.1f);
	Invoke("StopCrabageddonPowerUp",10.0f);
}

function StopCrabageddonPowerUp()
{
	CancelInvoke("changeColor");
	_pCtrl.isCrabbageddon = false;
	playerTextureManager.ResetCrabTextureArray();
	TurnCrabColorChange(false);
	TurnCrabSpeed(false);
	Destroy(gameObject.GetComponent("CrabageddonPowerUp"));
}