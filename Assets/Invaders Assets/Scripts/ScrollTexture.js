// Scrolls texture offset over time

var scrollspeed = .025;
private var offset = Vector2.zero;

function Update () {
	offset.y += scrollspeed * Time.deltaTime;
	renderer.material.SetTextureOffset ("_MainTex", offset);
}
