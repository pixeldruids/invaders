
var uvAnimationTileX :int ; //Here you can place the number of columns of your sheet. 
                           //The above sheet has 24

var uvAnimationTileY :int; //Here you can place the number of rows of your sheet. 
                          //The above sheet has 1
var framesPerSecond :float ;

var playOnce : boolean ;

var isPlayOnceFinished : boolean;

 var index : int =0;
 
function Start()
{
	InvokeRepeating("updateTiming",0.1f,0.1f);
} 
 
function updateTiming()
{
	index++;
}


function resetFields()
{
	index = 0;
	isPlayOnceFinished = false;
	playOnce = true;
	InvokeRepeating("updateTiming",0.1f,0.1f);
}


function Update () {

    
    // repeat when exhausting all frames
    index = index % (uvAnimationTileX * uvAnimationTileY);
   
    // Size of every tile
    var size = Vector2 (1.0 / uvAnimationTileX, 1.0 / uvAnimationTileY);
    
    var offset :Vector2 ;
   
    if(playOnce)
    {
    	if(!isPlayOnceFinished)
    	{
    		// split into horizontal and vertical index
    		var uIndex = index % uvAnimationTileX;
    		var vIndex = index / uvAnimationTileX;
    		 if(uIndex == (uvAnimationTileX-1)){
    		 	uIndex = 3;
    		 	CancelInvoke("updateTiming");
    		 	isPlayOnceFinished = true;
    		 	}
    	}
    	else
    	{
    		uIndex = 3;
    	}
    }
    else
    {
    	// split into horizontal and vertical index
    	 uIndex = index % uvAnimationTileX;
    	 vIndex = index / uvAnimationTileX;	
    }
    
    offset = Vector2 (uIndex * size.x, 1.0 - size.y - vIndex * size.y);
    
    
    renderer.material.SetTextureOffset ("_MainTex", offset);
    renderer.material.SetTextureScale ("_MainTex", size);
}