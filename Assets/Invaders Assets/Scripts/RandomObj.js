#pragma strict
var prevIndex : int;

public var prefabArray:GameObject[];

function Start()
{
	EnableMenuInput();
	prevIndex = 0;
	createObjs();
	
}

function EnableMenuInput()
{
	if( (Application.loadedLevelName) == "Menu")
	{
		var tempComp :MenuInput =  GameObject.FindWithTag("MainCamera").GetComponent("MenuInput") as MenuInput;
		tempComp.enabled = true;
		Time.timeScale = 1;
	}
}
function createObjs()
{

	yield WaitForSeconds(1);
	while(1)
	{
		yield WaitForSeconds(3);
		var _index :int = Random.Range(0,3);
		while(prevIndex == _index)
		{
			_index = Random.Range(0,3);
		}
		prevIndex = _index;
		// Put PowerUp Chest randomly on left or right side of screen
		if (Random.value < .5) {
			var ufoLocation = 60.0;
			var ufoDirection = -1;
		}
		else {
			ufoLocation = -60.0;
			ufoDirection = 1;
		}
		
		var objectClone : GameObject = Instantiate(prefabArray[_index], Vector3(ufoLocation,25.22, -2.0), Quaternion.identity);
		objectClone.layer = LayerMask.NameToLayer("menuRandomObjectsLayer");
		//objectClone.renderer.material.mainTexture = TextureArray[_index];
		
		if((ufoDirection == 1) && (_index == 1))
			objectClone.transform.Rotate(Vector3(0,180 ,0));	
		else if((ufoDirection == -1) && (_index == 2))
			objectClone.transform.Rotate(Vector3(0,180 ,0));	
			
		while (objectClone && objectClone.transform.position.x >= -60.0 && objectClone.transform.position.x <= 60.0) {
			objectClone.transform.position.x += Time.deltaTime * 10 * ufoDirection;
			yield;
		}
		if (objectClone) {
			Destroy(objectClone);
		}
	}
}