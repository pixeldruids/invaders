#pragma strict
enum crabTextureEnum
{	
	CRABIDLE_L,
	CRABIDLE_R,
	CRABWALK_L,
	CRABWALK_R,
	CRABSHOOT_L,
	CRABSHOOT_R
}


public enum PlayerPosEnum
{
	PLAYERLEFT,    //0
	PLAYERRIGHT,   //1
	PLAYERSHOOT,   //2
	PLAYER_LEFT_IDLE, //3
	PLAYER_RIGHT_IDLE, //4
	NONE
};

private var xValue:int;   //JoyStick XValue
private var preXValue :int = 2;  // JoyStick Previous XValue

public var PlayerTextureArray : Texture2D[];
private var TextureArray : Texture2D[];

private var CurrPos:PlayerPosEnum  = PlayerPosEnum.PLAYER_LEFT_IDLE;
private var PrevPos:PlayerPosEnum  = PlayerPosEnum.PLAYER_LEFT_IDLE;
private var missileStart:boolean = false;
private var player_AnimatedTextureUV : CrabAnimatedTextureUV ;
		
function Start () 
{
	TextureArray = new Texture2D[6];
	ResetCrabTextureArray();
	ApplyPlayerTexture(PlayerTextureArray[CurrPos]);
	player_AnimatedTextureUV = gameObject.GetComponent("CrabAnimatedTextureUV") as CrabAnimatedTextureUV;
	missileStart = false;
}

function Update () 
{
	
	if( (gameObject.GetComponent(PlayerControl) as PlayerControl).GetPlayerCanMoveStatus())
	{
		GetStatusOfJoyStickXValue();
		GetHorizontalJoyStick();
	}
}

function FixedUpdate()
{
	if( (gameObject.GetComponent(PlayerControl) as PlayerControl).GetPlayerCanMoveStatus())
	{
		changeInvaderTexture();
	}	
}



function GetStatusOfJoyStickXValue()
{
	xValue =(gameObject.GetComponent(PlayerControl) as PlayerControl).GetCurrentStatusOfJoyStick();
}

function SetMissileStatus(_val: boolean)
{
	missileStart =_val; 
}

function ChooseCurrentDirection_caseZero(_prevXValue:int)
{	
	if(_prevXValue == 1)
	{
		if(xValue == 0)
			CurrPos =  PlayerPosEnum.PLAYER_LEFT_IDLE ;
		else
			CurrPos =  PlayerPosEnum.PLAYERLEFT ;
	}
	else if(_prevXValue == -1)
	{
		if(xValue == 0)
			CurrPos = PlayerPosEnum.PLAYER_RIGHT_IDLE;		
		else
			CurrPos = PlayerPosEnum.PLAYERRIGHT;		
	}
	
}


function GetHorizontalJoyStick()
{
	if(preXValue==2)
		preXValue = xValue;
		
	if( preXValue != xValue )
	{
		switch(xValue)
		{
			case 0:
					ChooseCurrentDirection_caseZero(preXValue);
					break;
			case 1: 
					SetLeftArrowKeyStatus(true);
					break;
			case -1:
					SetRightArrowKeyStatus(true);
					break;				
		}
		preXValue = xValue;
	}	
	
	if(missileStart)
	{
		missileStart = false;
		CurrPos = PlayerPosEnum.PLAYERSHOOT;
	}
	return 0;
}

function playIdle(_prevXValue:int)
{
	ChooseCurrentDirection_caseZero(_prevXValue);
}

function SetLeftArrowKeyStatus(_status : boolean)
{
	CurrPos =  PlayerPosEnum.PLAYERLEFT;
}

function SetRightArrowKeyStatus(_status : boolean)
{
	CurrPos = PlayerPosEnum.PLAYERRIGHT;
}


function ApplyPlayerTexture(_texture:Texture2D)
{
	gameObject.renderer.material.SetTexture("_MainTex", _texture);
}

function PlayerShootAnimationState()
{
	if(!player_AnimatedTextureUV.playOnce)
	{
		player_AnimatedTextureUV.changeAnimation(4,1,4,true,0);	
		if((PrevPos ==PlayerPosEnum.PLAYERLEFT) || ( PrevPos ==PlayerPosEnum.PLAYER_LEFT_IDLE )  )
			ApplyPlayerTexture(TextureArray[crabTextureEnum.CRABSHOOT_R]);
		else
			ApplyPlayerTexture(TextureArray[crabTextureEnum.CRABSHOOT_L]);
	}
	else
	{
		if(player_AnimatedTextureUV.isPlayOnceFinished)
		{
			player_AnimatedTextureUV.isPlayOnceFinished = false;
			if((PrevPos ==PlayerPosEnum.PLAYERLEFT) || ( PrevPos ==PlayerPosEnum.PLAYER_LEFT_IDLE ))// check whether either left idle or right idle
				playIdle(1);
			else
				playIdle(-1);
			player_AnimatedTextureUV.playOnce = false;
		}
	}
	
}




function changeInvaderTexture()
{
		switch(CurrPos)
		{
			case PlayerPosEnum.PLAYERSHOOT:
								PlayerShootAnimationState();
								break;
			case PlayerPosEnum.PLAYERLEFT:
								player_AnimatedTextureUV.changeAnimation(6,1,5,false,0);	
								ApplyPlayerTexture( TextureArray[crabTextureEnum.CRABWALK_L] );	
								PrevPos = CurrPos;
								CurrPos = PlayerPosEnum.NONE;
								break;
			case PlayerPosEnum.PLAYERRIGHT:
								player_AnimatedTextureUV.changeAnimation(6,1,5,false,0);	
								ApplyPlayerTexture( TextureArray[crabTextureEnum.CRABWALK_R] );	
								PrevPos = CurrPos;
								CurrPos = PlayerPosEnum.NONE;
								break;									
			case PlayerPosEnum.PLAYER_LEFT_IDLE:	
								player_AnimatedTextureUV.changeAnimation(5,1,5,false,0);	
								ApplyPlayerTexture(TextureArray[crabTextureEnum.CRABIDLE_L] );	
								PrevPos = CurrPos;
								CurrPos = PlayerPosEnum.NONE;						
								break;
			case PlayerPosEnum.PLAYER_RIGHT_IDLE:	
								player_AnimatedTextureUV.changeAnimation(5,1,5,false,0);	
								ApplyPlayerTexture(TextureArray[crabTextureEnum.CRABIDLE_R]);	
								PrevPos = CurrPos;
								CurrPos = PlayerPosEnum.NONE;						
								break;					
			case PlayerPosEnum.NONE:
								break;						
		}
}

function AssignCrabTextureArray(_TextureArray:Texture2D[])
{
	TextureArray = _TextureArray.Clone();
	CurrPos  = PrevPos;	
}


function ResetCrabTextureArray()
{
	AssignCrabTextureArray(PlayerTextureArray);
}

function TurnCrabColors(_color : Color)
{
	
	if(_color == Color.white)
	{
		ResetCrabTextureArray();
	}
	gameObject.renderer.material.color = _color ;//Color.white;
}
