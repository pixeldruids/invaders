// Behavior for a damaged alien

var alienExplosion : GameObject;

function OnCollisionEnter () {
	Instantiate(alienExplosion, transform.position, Quaternion.identity);
//	particleEmitter.emit = false;	// The autodestruct on the particle system will destroy us when it runs out
	transform.position.y += 1000.0;	// Make sure nothing else can hit us in the meantime by moving out of the way
	rigidbody.Sleep(); // And don't be moving about either
}