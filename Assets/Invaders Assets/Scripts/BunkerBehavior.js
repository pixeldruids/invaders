// Controls what happens when various objects hit the bunker

private var newTexture : Texture2D;
private var hit : RaycastHit;
@HideInInspector
var bunkerTexture : Texture2D;	// Assigned by the InvaderControl script

function OnDisable () {
	Destroy(bunkerTexture);	// Since this is uniquely Instantiated in the InvaderControl script, it won't just disappear on its own.
							// Therefore we need to manually clean it up.  Garbage collection only applies to standard Mono objects
							// like ints, floats, and arrays, not Unity objects like GameObjects, Texture2D, etc.
}

function OnTriggerEnter (other : Collider) {
	// Get rid of bunker if an invader hits it
	if (other.tag == "alien") {
		Destroy (gameObject);
		BunkerControl.use.Explode (transform.position);
	}
}

function OnTriggerStay (other : Collider) {
//	Debug.Log("OnTriggerStay = "+other.transform.position);
	var hitpoint : Vector3 = other.transform.position - Vector3.forward*2.0;
	// Check a few slightly different points around the missile/bomb position for more accurate collision detection
	if (!CheckPoint (hitpoint, other) ) {
		if (!CheckPoint (hitpoint + Vector3.up*.01, other) ) {
			CheckPoint (hitpoint - Vector3.up*.01, other);
		}
	}	
}

function CheckPoint (hitpoint : Vector3, other : Collider) : boolean {
	// If A) the raycast hits and B) the point hit is a solid color and not clear, then something hit us
	if (Physics.Raycast (hitpoint, Vector3.forward, hit, 5.0) && BunkerControl.use.CheckAlpha (bunkerTexture, hit.textureCoord) ) {
		// In which case, explode either the missile or the bomb
		if (other.name == "player1Missile") {
			Debug.Log("player1 missle");
			(other.GetComponent(MissileControl) as MissileControl).Explode(1,other.name);
		}
		else if (other.name == "player2Missile") {
			(other.GetComponent(MissileControl) as MissileControl).Explode(2,other.name);
		}

		else {
			if(other.GetComponent(Bomb) != null)
			{
				(other.GetComponent(Bomb) as Bomb).ExplodeOnBunkers();
			}
		}
		return true;
	}
	else {
		return false;
	}
}