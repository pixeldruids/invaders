#pragma strict

var gotFirstBonusLife:boolean = false;
var nextBonusLifeScore:int ;
var bonusLifeScore = 10000;

function Start () {	
	nextBonusLifeScore = bonusLifeScore/2;  // for 5000
	}
	
function CalculateExtraLife(_score:int):int
{
	if(_score>0)
	{
		if((!gotFirstBonusLife) && (_score >= nextBonusLifeScore) )
		{
			nextBonusLifeScore = bonusLifeScore ;
			gotFirstBonusLife = true;
			return 1;
		}
		if( _score >= nextBonusLifeScore){
			nextBonusLifeScore = bonusLifeScore * ((nextBonusLifeScore / bonusLifeScore ) +1) ;
			return 1;
		} 		
	 	else 
	 		return 0;	
	 }
	return 0; 	
}
