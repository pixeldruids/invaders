#pragma strict
var targetObject:GameObject;
var colorProperty:String;
var isFlashing:boolean;
private var targetMaterial:Material;
private var originalAlpha:float;
private var originalColor:Color;
private var alpha:float;
private var sign:int;
function Start () {
	targetMaterial = targetObject.renderer.material;
	isFlashing = false;
}

function Update () {

}

function startFlashing(time:float)
{
	targetMaterial = targetObject.renderer.material;
	originalColor = targetMaterial.GetColor(colorProperty);
	originalAlpha = originalColor.a;
	alpha = originalAlpha;
	sign = -1;
	isFlashing = true;
	InvokeRepeating("updateAlpha",0.0f,0.1f);
	Invoke("stopFlashing",time);
}

function stopFlashing()
{
	isFlashing = false;
	resetAlpha();
	CancelInvoke("updateAlpha");
}
function updateAlpha()
{
	var matColor:Color = originalColor;
	alpha = alpha + (1 * sign); 
	if(alpha > 1.0f)
	{ 
		alpha = 1.0f;
		sign = -1;
	}
	if(alpha < 0.2f)
	{
		alpha = 0;
		sign = 1;
	}
	matColor.a = alpha;
	targetMaterial.SetColor(colorProperty,matColor);;
}
function resetAlpha()
{
	targetMaterial.SetColor(colorProperty,originalColor);;
}