function OnPreRender () {
	RenderSettings.fog = false;
}
 
function OnPostRender () {
	RenderSettings.fog = true;
}
 
@script AddComponentMenu ("Rendering/Fog Layer")
@script RequireComponent (Camera)