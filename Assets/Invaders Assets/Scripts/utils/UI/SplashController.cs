using UnityEngine;
using System.Collections;

public class SplashController : MonoBehaviour 
{
	public GameObject[] screens;
	public GUITexture fadeCover;
	public Camera splashCamera;

	public Color startColor;
	public Color endColor;
	public float openFadeTime;

	public float screenEnterFadeTime = 1.0f;
	public float screenTime;
	public float screenExitFadeTime;

	public string loadLevel = "MainMenu";

	enum SplashState
	{
		colorFade,
		enterFade,
		screenShow,
		exitFade
	};

	private SplashState currentActivity;
	private float currentTime;
	private int currentScreen;

	// Use this for initialization
	void Start () 
	{
		currentActivity = SplashState.colorFade;
		currentTime = openFadeTime;
		currentScreen = 0;
		splashCamera.backgroundColor = startColor;
		Rect pi = fadeCover.pixelInset;
		pi.width = Camera.main.GetScreenWidth();
		pi.height = Camera.main.GetScreenHeight();
		fadeCover.pixelInset = pi;
		}
	
	// Update is called once per frame
	void Update () 
	{
		float interp =0;

		currentTime -= Time.deltaTime;
		if (currentTime <= 0.0f && currentScreen < screens.Length)
		{
			switch (currentActivity)
			{
				case SplashState.colorFade:
					{
						splashCamera.backgroundColor = endColor;
						Color color = fadeCover.color;
						color.a = 1f;
						fadeCover.color = color;
						fadeCover.gameObject.active = true;
						screens[currentScreen].active = true;
						currentTime = screenEnterFadeTime;
						currentActivity = SplashState.enterFade;
					}
					break;
				case SplashState.enterFade:
					{
						Color color = fadeCover.color;
						color.a = 0f;
						fadeCover.color = color;
						currentTime = screenTime;
						currentActivity = SplashState.screenShow;
					}
					break;
				case SplashState.screenShow:
					currentTime = screenExitFadeTime;
					currentActivity = SplashState.exitFade;
					break;
				case SplashState.exitFade:
					{
						Color color = fadeCover.color;
						color.a = 1f;
						fadeCover.color = color;
						screens[currentScreen++].active = false;
						if (currentScreen < screens.Length)
						{
							screens[currentScreen].active = true;
						}
						else
						{
							Application.LoadLevel(loadLevel);
						}
						currentTime = screenEnterFadeTime;
						currentActivity = SplashState.enterFade;
					}
					break;
			}
		}
		else
		{
			Color color = fadeCover.color;
			switch (currentActivity)
			{
				case SplashState.colorFade:
					interp = (openFadeTime - currentTime) / openFadeTime;
					splashCamera.backgroundColor = (interp * endColor) + ((1f - interp) * startColor);
					break;
				case SplashState.enterFade:
					interp = (screenEnterFadeTime - currentTime) / screenEnterFadeTime;
					color.a = 1f - interp;
					break;
				case SplashState.exitFade:
					interp = (screenExitFadeTime - currentTime) / screenEnterFadeTime;
					color.a = interp;
					break;

			}
			fadeCover.color = color;
		}	
	}
}
