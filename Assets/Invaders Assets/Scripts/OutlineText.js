// Creates outline text from standard mono-colored fonts by creating 4 clones offset slightly
// This can be set up manually of course, but then it's a pain when you want to change anything

var outlineColor = Color.black;
var outlineDistance = .0002;

function Start () {
	var offsetPosition = new Vector3[4];
	offsetPosition[0].x = -outlineDistance;
	offsetPosition[1].x = outlineDistance;
	offsetPosition[2].y = -outlineDistance;
	offsetPosition[3].y = outlineDistance;
	// If we set textClone.guiText.material.color (in the loop below), Unity would be "helpful" and create a new material for us behind the scenes
	// for each object we use. But that would make 4 materials in this case, and we only need 1, so we'll manually instantiate a material ourselves
	var outlineMaterial = Instantiate(guiText.material);
	outlineMaterial.color = outlineColor;
	
	for (var i = 0; i < 4; i++) {
		offsetPosition[1].z = -1.0;
		var textClone = Instantiate(gameObject, transform.position + offsetPosition[i], Quaternion.identity);
		DestroyImmediate(textClone.GetComponent(OutlineText));	// Otherwise it's infinite recursion time
		textClone.guiText.text = guiText.text;
		textClone.guiText.material = outlineMaterial;
	}
}