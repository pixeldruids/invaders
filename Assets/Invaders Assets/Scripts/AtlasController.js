#pragma strict

var currentSelected:int;
var invaderCtrl:InvaderControl;
var ButtonList:buttonBehaviour[] = new buttonBehaviour[2];

var menuSelectionSound:AudioClip;
var menuEnterSound:AudioClip;

var isPaused : boolean;

function Start () {
		SetPosition();
		isPaused = false;
		currentSelected = 1;
		InputManager.Instance.greenThrottle.Reset();
		SetOthersInput(false);
}

function SetPosition()
{
	gameObject.transform.position = Vector3(2.5,0.4,-63.12);
}

function SetOthersInput(_val:boolean)
{
	if( (Application.loadedLevelName) == "Menu")
	{
		var tempComp :MenuInput =  GameObject.FindWithTag("MainCamera").GetComponent("MenuInput") as MenuInput;
		tempComp.enabled = _val;
	}
	
	if((Application.loadedLevelName) == "MainGame")
	{
		if(GameObject.FindWithTag("Player"))
		{
			var _playerCtrl :PlayerControl = GameObject.FindWithTag("Player").GetComponent("PlayerControl") as PlayerControl;
			_playerCtrl.enabled = _val;
		}
		if(GameObject.Find("Paused"))
		{
			var _pauseJs : pauseJS = GameObject.Find("PauseGUI").GetComponent("pauseJS") as pauseJS;
			_pauseJs.enabled = _val;
			isPaused = true;
		}
		
		if(GameObject.Find("GameOverGUI"))
		{
			var _gOver : gameOverBehaviour = GameObject.Find("GameOverGUI").GetComponent("gameOverBehaviour") as gameOverBehaviour;
			_gOver.enabled = _val;
		}
	}
	
	if((Application.loadedLevelName) == "Settings")
	{
		var settingsBvr:settingsBehaviour = GameObject.Find("BG").GetComponent("settingsBehaviour") as settingsBehaviour;
		settingsBvr.enabled = _val;
	}
}

function PlayMenuSelectionSound()
	{
		audio.clip = menuSelectionSound;
		audio.Play();
	}
	
	function PlayMenuEnterSound()
	{
		audio.clip = menuEnterSound;
		audio.Play();
	}
	
function Update () 
{
	if(InputManager.Instance.GetKeyDown(InputManager.IB_PREV))
	{
		currentSelected = 1;
		makeDecisionOnSelection();
	}
	
	if(InputManager.Instance.GetKeyDown(InputManager.IB_SELECT)||  (Input.GetKeyDown(KeyCode.Escape)))
	{
		currentSelected = 1;
		makeDecisionOnSelection();
		Invoke("loadScene",0.2);
	}
	
	if(Input.GetKeyDown(KeyCode.LeftArrow) || InputManager.Instance.GetKeyDown(InputManager.IB_LEFT))
	{
		--currentSelected;
		PlayMenuSelectionSound();
	}
	if(Input.GetKeyDown(KeyCode.RightArrow) || InputManager.Instance.GetKeyDown(InputManager.IB_RIGHT))
	{
		++currentSelected;
		PlayMenuSelectionSound();
	}
	if(currentSelected > 1) currentSelected = 0;
	if(currentSelected < 0) currentSelected = 1;
	
	if(Input.GetKeyDown(KeyCode.Space) || InputManager.Instance.GetKey(InputManager.Instance.IB_FIRE))
	{			
		makeDecisionOnSelection();
	}		
	
	for(var i = 0; i < ButtonList.Length; i++)
	{
		if(currentSelected == i) ButtonList[i].setSelected();
		else ButtonList[i].setNotSelected();
	}
		
		
}

function loadScene()
{
	if( (Application.loadedLevelName) == "MainGame")
	{
		if(!(GameObject.Find("Paused")))
			Application.LoadLevel("Menu");
	}
	else {
		Application.Quit();	
		}
}

function Hide()
{
	SetOthersInput(true);
	if(Application.loadedLevelName=="MainGame")
	{
		if(!isPaused){
			Utility.SetVolumeForPauseScreen(1);
			Time.timeScale = 1;  // activate timer 
		}
	}
	else
	{
		Utility.SetVolumeForPauseScreen(1);
		Time.timeScale = 1; 
	}		
	this.gameObject.SetActiveRecursively(false);
}
	

function makeDecisionOnSelection()
{
	Time.timeScale = 1;  // activate timer 
	if(currentSelected == 0)
	{
		PlayMenuEnterSound();
		yield WaitForSeconds(0.2);
		Application.Quit();
	}
	if(currentSelected == 1)
	{
		PlayMenuEnterSound();
		yield WaitForSeconds(0.2);
		Time.timeScale = 0;  // activate timer 
		SetOthersInput(true);
		Hide();
	}
	

}
	
