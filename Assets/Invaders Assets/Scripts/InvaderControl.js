
var invaders : GameObject[];
var invaderColors : Color[];
var invaderMaterials : Material[];
private var sharedInvaderMaterials : Material[];
var ufo : GameObject;
var powerUpChest : GameObject;
var bunker1 : GameObject;
var bunker2 : GameObject;
var base : GameObject;
var base2:GameObject;
var iBomb : GameObject;
var rows = 5;
var columns = 11;
var startHeight = 9;
var iSpeed = 1.5;
var speedIncrease = 1.6;
private var iStartSpeed : float;
static var iDirection : int;
var ufoSpeed = 10.0;
var bombSpeed = 10.0;

private var powerUp_probability_chance :int = 1;
private var powerUp_cycle :int = 4;

var scoreObject : TextMesh;
var score1Object : TextMesh;

var pausedObject : GameObject;
var ufoPoints : GUIText;
var gameOverObject : GameObject;
var alienPoints = new Array (10, 20, 30, 40, 50);
var alienExplosion : GameObject;
var alienExplosion1 : GameObject;
var ufoExplosion : GameObject;
var alienExplosionBySharkPowerUp : GameObject;


public var invaderClone : Transform[];
private var invaderParent : Transform;
private var invaderMaterial : Material[];
private var movingDown = false;
private var numberLeft : int;
private var speedupNumber : int;
private var firstSpeedup : boolean;
private var twoBombs : boolean;
public var invaderArray : int[];
var bunkerParent : GameObject;
private var bunkers : GameObject[];
private var bunkerMaterials : Material[];
private var boundaries : Collider[];

private var score = 0;
private var score1 = 0;

private var iAnimFrame = 0.0;
private var animateInterval : float;
private var animateTimer : float;

public var player : GameObject;
static var playerTransform : Transform;
var extraLives:int = 2;
var playerLife : GameObject;
var extraLife : GameObject;

public var player1 : GameObject;
static var playerTransform1 : Transform;
var extraLives1:int = 2;
var playerLife1 : GameObject;
var extraLife1 : GameObject;


private var level : int;
private var levelInProgress : boolean;
public var gameOver : boolean;
static var levelEnded : boolean;

var invaderMoveSound : AudioClip;
var invaderLandSound : AudioClip;
var levelSwoosh : AudioSource;
var bunkerRegrowth : AudioClip;
var freezeSound :AudioClip;

var cam : Camera;
static var use : InvaderControl;

private var bombAnimFrame = .5;
private var sharedBombMaterial : Material;
var bombMaterial : Material;
var bombAnimateInterval = .15;

var isFlipY: boolean;
var gameMode: int;
var crab1Image: GameObject;
var crab2Image: GameObject;
var hud : GameObject;

var powerUpIcon : GameObject ;
var powerUpIconTextureArray : Texture2D[];
static var invaderFreeze:boolean = false;
var playerPosition : Vector3 = Vector3(6.0, -8.16, 0.0);
var player1Position : Vector3 = Vector3(60.0,  -8.16, 0.0);
var invaderTimerToChangeTexture : float =0;
public var invaderYPosArray :int[];
private var invaderDummyArray : int[];
static var prePowerUpType : int = 100;
var chestExplosion : GameObject;
private var isFirstTime:boolean ;
private var powerUpAppearancePos :int;
var playerControl:PlayerControl;
var player1Control:PlayerControl;

var OneUp:GameObject;


function Awake () {
	Time.timeScale = 1.0;
	use = this;	// Allows easy access to functions etc. in this script without having to get a reference, since it's a static var
	isFirstTime = true;

}

function Start () {
	
	Utility.LoadVolume();
	gameMode = InputManager.gameMode;
	isFlipY = false;
	// Get references to boundaries so we don't have to do GameObject.Find like this more than once
	boundaries = new Collider[2];
	boundaries[0] = GameObject.Find("BoundaryL").collider;
	boundaries[1] = GameObject.Find("BoundaryR").collider;
	
	// This empty transform will be the parent of all invaders, so we can just move the parent and all invaders will move with it
	invaderParent = new GameObject("Invader Parent").transform;
	
	// Set up materials for each invader type, which will use these as sharedMaterials, so animating these will animate all invaders
	// The colors are added separately like this because we want the occasional destroyed alien to still be just gray
	sharedInvaderMaterials = new Material[invaders.Length];
	for (var i = 0; i < invaders.Length; i++) {
		sharedInvaderMaterials[i] = Instantiate(invaderMaterials[i]);
		sharedInvaderMaterials[i].color = invaderColors[i];
	}
	// Also set up bomb material for the same reason...animating this will animate all bombs
	sharedBombMaterial = Instantiate(bombMaterial);

	iStartSpeed = iSpeed; // Keep track of the original invader movement speed
	movingDown = false;
//	audio.pitch = 1.0;
//	audio.clip = invaderMoveSound;
	score = 0;
	player = Instantiate(base);	
	player.name = "player1";
	playerTransform = player.transform;

	player.renderer.enabled = false;
	crab1Image.renderer.enabled = true;
	crab2Image.renderer.enabled = false;
	if(gameMode != 0)
	{
		player1 = Instantiate(base2);
		player1.name = "player2";
		playerTransform1 = player1.transform;
		player1.renderer.enabled = false;
		crab2Image.renderer.enabled = true;
	}
	level = 1;
	levelInProgress = false;
	gameOver = false;
	twoBombs = false;

	AddScore(0,"init");	// Just to get it on-screen
	yield SetUpInvaders(0.0);
	InitializeLevel(0.0);	// Levels after the first one do some other stuff after the invaders are set up, which is why these are separate functions
	hud = GameObject.FindWithTag("hud");
	hud.gameObject.SetActiveRecursively(true);
	PlayGame();
	
}

function GetInvaderYPosValue(_val : int) :int
{
	return invaderYPosArray[_val];
}

function TurnInvaderAinmation(_status : boolean)
{

	for (var i = 0; i < numberLeft; i++) 
	{
		if (invaderClone[invaderArray[i]]) 
		{
			invaderClone[invaderArray[i]].gameObject.GetComponent.<AnimatedTextureUV>().enabled = _status;		
		}
	}
}

function TurnInvaderColors(_color : Color)
{

	for (var i = 0; i < numberLeft; i++) 
	{
		if (invaderClone[invaderArray[i]]) 
		{
			invaderClone[invaderArray[i]].gameObject.renderer.sharedMaterial.color = _color;		
		}
	}			
}



function RegrowthBunkers()
{
	reGrow();
	for (var i = 0; i < bunkers.length; i++) 
	{
		if (bunkers[i]) {	// These can potentially be destroyed if invaders touch them, so we have to check to see if they exist
			var flash: flashBehaviour =  bunkers[i].GetComponent(flashBehaviour);
			flash.startFlashing(1);
		}
	}	
	audio.clip = bunkerRegrowth;
	audio.Play();
}

function reGrow()
{
	for (var i = 0; i < bunkers.length; i++) 
	{
		if (bunkers[i]) {	// These can potentially be destroyed if invaders touch them, so we have to check to see if they exist
			Destroy(bunkers[i]);		
		}
	}
	SetUpBunkers(0.0);
}

function SetUpBunkers(zDistance : float)
{
	// Set up bunkers...each one will have an instantiated material assigned to it
	// Since each bunker will write to its own texture at runtime, we want to work off a copy of the texture and not use the same one for them all
	if (startHeight > 2) {	// Eventually the invaders will be low enough that we won't use bunkers at all
		bunkers = new GameObject[4];
		bunkerMaterials = new Material[4];
		for (var i = 0; i < 4; i++) {
			if( (i % 2) == 0)
				bunkers[i] = Instantiate(bunker1, Vector3(i*15.0 + 3.0, -1.0, zDistance), Quaternion.identity);
			else
				bunkers[i] = Instantiate(bunker2, Vector3(i*15.0 + 3.0, -1.0, zDistance), Quaternion.identity);
			// If we change an object's renderer.material, Unity makes a "scene material" copy for us behind the scenes. This is usually helpful, but
			// unfortunately it tends to hide the actual complexity of using materials (which I guess is the point).  In this case, we want to keep
			// a direct reference to the scene materials so we can destroy them as necessary.  Otherwise scene materials keep building up and using
			// memory every time we make new bunkers, since we need a unique material for each one.  So we instantiate the materials ourselves instead
			// of letting Unity do it.  The excess scene materials would be destroyed anyway when loading a new level, so it's not necessarily fatal or
			// anything, but it's often better to handle it like this, and definitely in cases where you could theoretically have an unlimited number
			// of unique scene materials. (Which is not the case here since eventually bunkers stop being created when the invaders are low enough, but
			// it's the principle of the thing.)
			bunkerMaterials[i] = Instantiate(bunkers[i].renderer.sharedMaterial);
			bunkers[i].transform.parent = bunkerParent.transform;
			bunkers[i].renderer.material = bunkerMaterials[i];
			var bunkerScript : BunkerBehavior = bunkers[i].GetComponent(BunkerBehavior);
			bunkerScript.bunkerTexture = Instantiate(bunkerMaterials[i].mainTexture);
			bunkerMaterials[i].mainTexture = bunkerScript.bunkerTexture;

		}
	}
	
}

function SetUpInvaders (zDistance : float) 
{
	SetUpBunkers(zDistance);
	invaderYPosArray = new int[5];
	// Set up invaders
	invaderClone = new Transform[columns * rows];	// Mostly we'll refer to the invaderClone's Transform component, so it's faster to use this
													// rather than having an array of GameObjects
	invaderParent.position = Vector3(0.0, 1000.0, zDistance);
	var rowCounter = 0;
	var invaderNum = 0;
	for (var y = 0; y < rows; y++) {
		var invaderType = y;
		for (var x = 0; x < columns; x++) 
		{
			invaderClone[invaderNum] =
				Instantiate(invaders[invaderType], Vector3((x*5.0)+3.0, -5.0+(y*2.0+startHeight)*3.0, zDistance), Quaternion.identity).transform;
			invaderClone[invaderNum].parent = invaderParent;
			invaderClone[invaderNum].gameObject.renderer.sharedMaterial = sharedInvaderMaterials[invaderType];
			invaderClone[invaderNum].gameObject.name = invaderType.ToString();	// Used to identify invader types when shot so the score can be computed
			invaderNum++;
			if (level == 1) {	// For the first level, make invaders appear in sequence instead of all at once
				yield WaitForSeconds(.01);
			}
		}
		
		invaderYPosArray[y] = -5.0+(y*2.0+startHeight)*3.0;	
	}

	invaderArray = new int[columns * rows];	// Used primarily for calculating bomb drops in the Bomb function
	invaderDummyArray = new int[columns * rows];
	BuildInvaderDummyArray() ;
	BuildInvaderArray();
	StartPlayer();
}



private var invaderArrayCount : int;

function BuildInvaderDummyArray() {
	yield;	// The array is rebuilt every time an invader is destroyed, but Destroy() doesn't happen until the end of the frame
			// Therefore we need to wait a frame for the Destroy() to take effect, so this array will be accurate
	invaderArrayCount = 0;
	// Build array of existing invaders so we can pick one that will drop a bomb
	for (var i = 0; i < rows*columns; i++) {
		if (invaderClone[i]) {
			invaderDummyArray[invaderArrayCount++] = i;
		}
	}
}

function BuildInvaderArray () {
	yield;	// The array is rebuilt every time an invader is destroyed, but Destroy() doesn't happen until the end of the frame
			// Therefore we need to wait a frame for the Destroy() to take effect, so this array will be accurate
	invaderArrayCount = 0;
	// Build array of existing invaders so we can pick one that will drop a bomb
	for (var i = 0; i < rows*columns; i++) {
		if (invaderClone[i]) {
			invaderArray[invaderArrayCount++] = i;
		}
	}
}

function InitializeLevel (zDistance : float) {	// zDistance is the z position at which the board is set up

	if(GameObject.FindWithTag("powerUpIcon"))
		Destroy(GameObject.FindWithTag("powerUpIcon"));
																								// It will be 0 for the first level and 1000 for the others, because of the "zoom" effect at level end
	iAnimFrame = 0.0;	// Alternates between 0 and .5; the animation frame of the invaders
	animateInterval = .95;	// Time interval between animation frames
	animateTimer = 1.0;	
	numberLeft = rows*columns;
	speedupNumber = numberLeft*.6;
	firstSpeedup = true;
	iSpeed = iStartSpeed;
	iDirection = 1;
	// Move invaders/bunkers to starting position (used for level transitions)
	if (bunkers[0]) {
		for (bunker in bunkers) {
			bunker.transform.position.z -= zDistance;
		}
	}
	invaderParent.position.z -= zDistance;
	invaderParent.position.y = 1000.0;
	
	if (level != 1) {	// A bit more time between levels
		yield WaitForSeconds(1.0);
	}
	
	BombControl(3.5);
	levelInProgress = true;
	levelEnded = false;
	
}

function isLevelEnded()
{
	return levelEnded;
}

function SetInvaderFreeze(_status:boolean)
{
	invaderFreeze = _status;
	
	if(_status){
		audio.clip = freezeSound;
		audio.loop = false;
		audio.Play();
		
	}
	else
	{
		audio.Stop();
	}
}


function GeneratePowerUpPosition() 
{
	powerUpAppearancePos = Random.Range(3,5);
}




function PlayGame () 
{
	// Have a random delay between UFOs
	var ufoTimer : float = Random.Range(20, 30);
	GeneratePowerUpPosition() ;
	 
	// This can be considered as the "main loop"
	while (!gameOver) 
	{				
		if (!movingDown && !(playerControl.playerDead && player1Control.playerDead)  && levelInProgress) 
		{
			if(!invaderFreeze)
				MoveInvadersAcross();	
			ExtraLife();	
		}
		
	//	Debug.Log(" Player.transform.position = "+player.transform.position);
		
		// Only advance UFO timer while the game is in progress
		if (levelInProgress) 
		{
			ufoTimer -= Time.deltaTime;
			if(powerUp_probability_chance > powerUp_cycle){   // reset variable once finished a cycle 
					powerUp_probability_chance = 1;
					GeneratePowerUpPosition() ;
			}
				
			if (ufoTimer < 0.0) 
			{
				ufoTimer = Random.Range(20, 30);
				// **************  displaying powerup chest after first octopus appearance ****************
				if(isFirstTime)
				{   
					powerUpAppearancePos = 2;
					if(powerUp_probability_chance>1)
						isFirstTime = false;
				}
				// ************** End ****************
				
				if(powerUp_probability_chance == powerUpAppearancePos){
					MakePowerChest ();  
				}else
					MakeUFO();
	
				powerUp_probability_chance ++;
			}
		}
		
		if (Input.GetButtonDown("Reset")) {
			Application.LoadLevel("MainGame");
		}
		
		yield;
	}

	// Game Over
	levelInProgress = false;
	playerControl.playerCanMove = false;
	player1Control.playerCanMove = false;
	DestroyPowerUp_UFO();
	Utility.score = score;
	Utility.score1 = score1;
	bunkerParent.gameObject.SetActiveRecursively(false);
	invaderParent.gameObject.SetActiveRecursively(false);
	player.gameObject.SetActiveRecursively(false);
	
	if(InputManager.gameMode==1)
		player1.gameObject.SetActiveRecursively(false);
		
	gameOverObject.gameObject.SetActiveRecursively(true);
	hud.gameObject.SetActiveRecursively(false);
	
	
	if( (extraLives <= -1) || (extraLives1 <= -1))
	{
		var _powerUpCtrl :powerUpControl = GameObject.Find("_Manager").GetComponent("powerUpControl");
		_powerUpCtrl.DestroyPowerUp();
	}	
	
	powerUp_probability_chance = 1;
    
	if(GameObject.FindWithTag("powerUpIcon"))
		Destroy(GameObject.FindWithTag("powerUpIcon"));
}


function DestroyPowerUp_UFO()
{
	while(1)
	{
		yield;
		if(GameObject.FindWithTag("ufo")){
			DestroyImmediate(GameObject.FindWithTag("ufo"),true);
		}
		else
			break;
	}	
	while(1)
	{
		yield;
		if(GameObject.FindWithTag("powerupchest")){ 
			DestroyImmediate(GameObject.FindWithTag("powerupchest"),true);	
		}
		else
			break;
	}
	while(1)
	{
		yield;
		if(GameObject.FindWithTag("powerUpIcon")){ 
			DestroyImmediate(GameObject.FindWithTag("powerUpIcon"),true);
		}
		else
			break;
	}				
}

function SetUfoSoundStatus(_val:boolean)
{
	var ufoObject : GameObject = GameObject.FindWithTag("ufo");
	if(ufoObject)
	{
		var ufoAudio : AudioSource= (ufoObject.GetComponent("AudioSource")) as AudioSource;
		if(_val)
			ufoAudio.Pause();
		else
			ufoAudio.Play();	
	}
}

function PauseGame()
{
	Time.timeScale = 1.0-Time.timeScale;	// Toggle between 1 and 0
	pausedObject.active = !pausedObject.active;
}




function StartPlayer () 
{		
		if(gameMode == 0)
		{
			player.transform.position = playerPosition;
			player.renderer.enabled = true;
			playerControl.playerCanMove = true;
			playerControl.playerDead = false;
		}
		if(gameMode != 0)
		{
			player.transform.position = playerPosition;
			if(extraLives <= -1)  //if player1 life goes to 0
			{
				player.transform.position -= Vector3.forward*20.0; // Move player back so there's no chance of getting hit by another bomb
				player.renderer.enabled = false;
				playerControl.playerCanMove = false;
				playerControl.playerDead = true;
			}
			else
			{
				player.renderer.enabled = true;
				playerControl.playerCanMove = true;
				playerControl.playerDead = false;
			}
		
			player1.transform.position =player1Position ;
			if(extraLives1 <=-1)//if player2 life goes to 0
			{
				player1.transform.position -= Vector3.forward*20.0; // Move player back so there's no chance of getting hit by another bomb
				player1.renderer.enabled = false;
				player1Control.playerCanMove = false;
				player1Control.playerDead = true;
			}
			else
			{
				player1.renderer.enabled = true;
				player1Control.playerCanMove = true;
				player1Control.playerDead = false;
			}
		}
		else
		{
			player1Control.playerDead = true;
			extraLives1 = -1;
		}
	
}
function StartPlayerNo (playerNo : int) {

	if(playerNo == 1)
	{
		player.transform.position = playerPosition;
		player.renderer.enabled = true;
		playerControl.playerCanMove = true;
		playerControl.playerDead = false;
		player.renderer.material.color = Color.white;
		SetPlayerCollisionStatus(false,player);
	}
	if(playerNo == 2)
	{	
		player1.transform.position = player1Position;
		player1.renderer.enabled = true;
		player1Control.playerCanMove = true;
		player1Control.playerDead = false;
		player1.renderer.material.color = Color.white;
		SetPlayerCollisionStatus(false,player1);
	}
	
}

function MakePowerChest () 
{
	// Put PowerUp Chest randomly on left or right side of screen
	if (Random.value < .5) {
		var ufoLocation = 90.0;
		var ufoDirection = -1;
	}
	else {
		ufoLocation = -30.0;
		ufoDirection = 1;
	}
	var chestClone : GameObject = Instantiate(powerUpChest, Vector3(ufoLocation, 49.50, 0.0), Quaternion.identity);

	/// If it hasn't been destroyed, keep moving it until it goes out of bounds
	while (chestClone && chestClone.transform.position.x >= -30.0 && chestClone.transform.position.x <= 90.0) {
		chestClone.transform.position.x += Time.deltaTime * ufoSpeed * ufoDirection;
		yield;
	}
	if (chestClone) {
		Destroy(chestClone);
	}
}


function MakeUFO () {
	// Put UFO randomly on left or right side of screen
	if (Random.value < .5) {
		var ufoLocation = 90.0;
		var ufoDirection = -1;
	}
	else {
		ufoLocation = -30.0;
		ufoDirection = 1;
	}
	ufoSpeed = 10;
	var ufoClone : GameObject = Instantiate(ufo, Vector3(ufoLocation, 50.5, 0.0), Quaternion.identity);
	if(ufoDirection == -1)
	ufoClone.transform.Rotate(new Vector3(0,180 ,0));	
	// If it hasn't been destroyed, keep moving it until it goes out of bounds
	while (ufoClone && ufoClone.transform.position.x >= -30.0 && ufoClone.transform.position.x <= 90.0) {
		ufoClone.transform.position.x += Time.deltaTime * ufoSpeed * ufoDirection;
		yield;
	}
	if (ufoClone) {
		Destroy(ufoClone);
	}
}


function DestroyPowerUpChest(_powerUpChest : GameObject, source : String, explosionPosition : Vector3 ,playerName:String)
{
	Instantiate(chestExplosion, _powerUpChest.transform.position, Quaternion.identity);
	
	// We aren't going to actually destroy the UFO for a little bit, so move it back to be 100% sure it can't be hit again
	_powerUpChest.transform.position.z += 3.0;
	
	// Make the pitch go up and the volume go down
	_powerUpChest.renderer.enabled = false;

	Destroy(_powerUpChest);
	yield WaitForSeconds(0.5);
	InstantiatePowerUpIcon(explosionPosition,playerName);
	
} 


        
function InstantiatePowerUpIcon(explosionPosition : Vector3,playerName:String)
{

	var t_powerUpIcon : GameObject = Instantiate(powerUpIcon,explosionPosition, Quaternion.identity);
	t_powerUpIcon.transform.position.z = -1;
	var _poweruptype : int = Random.Range(0, 5);
	while(_poweruptype == prePowerUpType)
	{
		_poweruptype = Random.Range(0,5);
	}
	prePowerUpType= _poweruptype;
	t_powerUpIcon.renderer.material.mainTexture = powerUpIconTextureArray[_poweruptype];
	t_powerUpIcon.name =powerUpControl.ConvertToEnum(_poweruptype).ToString();

	t_powerUpIcon.rigidbody.useGravity = true;
	t_powerUpIcon.rigidbody.isKinematic = false;
	// Make sure it won't collide with the game boundaries (not fatal, just looks stupid)
	Physics.IgnoreCollision (boundaries[0], t_powerUpIcon.collider);
	Physics.IgnoreCollision (boundaries[1], t_powerUpIcon.collider);
	
}



function DestroyUFO (ufo : GameObject, source : String ) 
{
	var explode : GameObject = Instantiate(ufoExplosion, ufo.transform.position, Quaternion.identity);
	explode.particleEmitter.maxEmission = 55.0; // Make the UFO explosion somewhat bigger than the default alienExplosion
	explode.particleEmitter.maxEnergy = 4.0;
	explode.particleEmitter.maxSize = 3.0;
		
	// Choose random point value (unlike the real Space Invaders, where this depends on the number of missiles fired)
	var points = Random.Range(1, 5);
	if (points < 4) {
		points *= 50;
	}
	else {
		points = 300;
	}
	
	AddScore (points, source);
	
	if(ufo.transform.position.x > 0)
		ufo.transform.position = Vector3(ufo.transform.position.x- 5,ufo.transform.position.y-3,ufo.transform.position.z);
	else
		ufo.transform.position = Vector3(ufo.transform.position.x+ 5,ufo.transform.position.y-3,ufo.transform.position.z);
		
	// Print the points on-screen at the UFO's last position
	var pointsClone : GUIText = Instantiate(ufoPoints, cam.WorldToViewportPoint(ufo.transform.position), Quaternion.identity);
	pointsClone.text = points.ToString();
	Destroy(pointsClone, 2.0);
		
	// We aren't going to actually destroy the UFO for a little bit, so move it back to be 100% sure it can't be hit again
	ufo.transform.position.z += 3.0;
	
	// Make the pitch go up and the volume go down
	ufo.renderer.enabled = false;
	while (ufo && (ufo.audio.pitch < 3.0)) {
		ufo.audio.pitch += Time.deltaTime*1.5;
		//ufo.audio.volume = Mathf.Clamp01(ufo.audio.volume - Time.deltaTime*.4);;
		yield;
	}
	Destroy(ufo);
	
}

function updateInvadersStatus()
{
	var tIndex:int=0;
	var isDestroyedRow :boolean = true;
	
	for (var _x = 0; _x < rows; _x++) 
	{
		for (var _y = 0; _y < columns; _y++) 
		{
			if(invaderClone[(_x*columns)+_y] )
			{
				invaderYPosArray[tIndex] =  invaderClone[invaderDummyArray[(_x*columns)+_y]].position.y;
				isDestroyedRow = false;
				break;
			}
			else
			{
				continue;	
			}
			
		}	
		if(isDestroyedRow){
			if(!GameObject.Find("sharkpowerup")){				
				invaderYPosArray[tIndex] =  1000;
			}
		}
		tIndex ++;
		isDestroyedRow = true;
	}
			

}

function MoveInvadersAcross () {
	
	// Animate frames of invaders
	animateTimer += Time.deltaTime;
	if (animateTimer > animateInterval) {
		animateTimer = 0.0;
		iAnimFrame = .5 - iAnimFrame;
		for (var i = 0; i < sharedInvaderMaterials.Length; i++) {
			sharedInvaderMaterials[i].SetTextureOffset ("_MainTex", Vector2(iAnimFrame, 0.0) );
		}
	}
	
	if(iDirection == -1)
		isFlipY = true;
	else
		isFlipY = false;
			
	if(iDirection == -1 && isFlipY) {
		for (i = 0; i < numberLeft; i++) {
			if (invaderClone[invaderArray[i]]!=null) 
				invaderClone[invaderArray[i]].transform.rotation = Quaternion.Euler(0,180,0);
		}
		isFlipY = false;	
	}
	else if(iDirection == 1 && (!isFlipY) ){
		for (i = 0; i < numberLeft; i++) {
			if (invaderClone[invaderArray[i]]!=null)
				invaderClone[invaderArray[i]].transform.rotation = Quaternion.Euler(0,0,0);
		}
	}
	
	// Move all invaders
	invaderParent.Translate(Vector3.right * (iSpeed * iDirection * Time.deltaTime) );
}

function getInvadersYPosition() : float 
{
	return invaderParent.position.y ;
}

function MoveInvadersDown () {

	updateInvadersStatus();
	
	movingDown = true;
	
	// Move invaders down 3 units
	var t = 0.0;
	var startPosition = invaderParent.position.y;
	while (t < 1.0) {
		updateInvadersStatus();
		t += iSpeed * Time.deltaTime * 1.26;
		invaderParent.position.y = Mathf.Lerp(startPosition, startPosition-3.0, t);
		yield;
	}
		
	// See if an invader landed by checking the y position of all existing invaders
	for (var i = 0; i < numberLeft; i++) 
	{	
		if (invaderClone[invaderArray[i]] && invaderClone[invaderArray[i]].position.y <= -7.0) {
		gameOver = true;
		}
	}
	animateTimer = animateInterval; // Make the next frame happen immediately after resuming horizontal movement...looks better
	movingDown = false;
}

function BombControl (timer : float) {
	// Ideally we could say 'Invoke "BombControl", 2.5' or whatever, but Invoke doesn't work with coroutines
	// So we have to manually WaitForSeconds here instead
	yield WaitForSeconds(timer);
	
	// Keep dropping bombs when possible
	while (levelInProgress) {
		yield WaitForSeconds(0.4);
		if (twoBombs) {
			Bomb(.5);
			yield Bomb(2.5);
		}
		else {
			yield Bomb(.4);
		}
		yield;
	}
}

var accurateBombChance = .62;
var dInvaderFreeze:boolean = false;

function Bomb (timer : float) {
	yield WaitForSeconds(timer);
	
	// skip bomb when frreze power up is activated
	if((dInvaderFreeze) && (!invaderFreeze))
		yield WaitForSeconds(1.2);
	else if(invaderFreeze){
		dInvaderFreeze = invaderFreeze;
		return;
		}
			
	dInvaderFreeze = invaderFreeze;	
	
	// Slight chance that the level is not actually in progress when this function is called, in which case skip the whole routine
	if (!levelInProgress) {return;}
	
	// Some of the time, find the invader nearest the player on the x axis
	if (Random.value < accurateBombChance) {
		var nearestPos = 9999.9;
		var chosenInvader = 0;
		var thisPos : float;
		for (var i = 0; i < numberLeft; i++) {
			if (invaderClone[invaderArray[i]]) {
				thisPos = Mathf.Abs(invaderClone[invaderArray[i]].position.x - playerTransform.position.x);
				if (thisPos < nearestPos) {
					nearestPos = thisPos;
					chosenInvader = invaderArray[i];
				}
			}
		}
	}
	// The rest of the time, just pick a random invader
	else {
		chosenInvader = invaderArray[Random.Range(0, numberLeft)];
	}
	
	// If there are any invaders on any rows below, bomb from the bottom-most one instead (so invaders don't bomb each other)
	var count = chosenInvader;
	while (count >= columns) {	// Only check if it's not on the bottom row already
		count -= columns;
		if (invaderClone[count]) {
			chosenInvader = count;
		}
	}
	
	// Drop the bomb
	if (invaderClone[chosenInvader]) {	// Prevents null reference exceptions from ever happening (probably won't, but just in case)
		var bombClone : GameObject = Instantiate(iBomb, invaderClone[chosenInvader].position + Vector3(.2, -3.8, 0.0), Quaternion.identity);
		bombClone.rigidbody.velocity.y = -bombSpeed;
		bombClone.renderer.sharedMaterial = sharedBombMaterial;
		var bombCloneCollider = bombClone.collider;
		// Ignore collisions with invaders...if they're moving fast enough, they can get underneath a bomb dropped from higher up
		for (i = 0; i < numberLeft; i++) {
			if (invaderClone[invaderArray[i]]) {
				Physics.IgnoreCollision(bombCloneCollider, invaderClone[invaderArray[i]].gameObject.collider);
			}
		}
	
		// Wait around until bomb hits something and ceases to exist
		while (bombClone) {
			yield WaitForSeconds(0.01);
		}
	}
}



function CalculateExtraLife(score)
{
	var extraLifeCtrl : ExtraLifeController;
	extraLifeCtrl = GameObject.Find("score").GetComponent("ExtraLifeController") as ExtraLifeController;
	return extraLifeCtrl.CalculateExtraLife(score);
}

function CalculateExtraLife_Player2(score)
{
	var extraLifeCtrl : ExtraLifeController;
	extraLifeCtrl = GameObject.Find("score1").GetComponent("ExtraLifeController") as ExtraLifeController;
	return extraLifeCtrl.CalculateExtraLife(score);
}


function ExtraLife()
{
	if(CalculateExtraLife(score))
	{
		FlashScore(GameObject.Find("score"));
		Show1Up(player);
		ChangePlayerLifeIconTexture("score");
		extraLives = extraLives + 1;
		
	}
	if(CalculateExtraLife_Player2(score1))
	{
		FlashScore(GameObject.Find("score1"));
		Show1Up(player1);
		ChangePlayerLifeIconTexture("score1");
		extraLives1 = extraLives1 + 1;
	}
}

function Show1Up(_playerObj : GameObject)
{
	var _OneUp:GameObject;
	var shader1 : Shader = Shader.Find("Particles/Alpha Blended");
	OneUp.renderer.material.shader = shader1;
	
	if(_playerObj.name == "player1")
		OneUp.renderer.material.mainTexture = Resources.Load("1up_red");
	else
		OneUp.renderer.material.mainTexture = Resources.Load("1up_yellow");
		
	var _pos : Vector3 = Vector3(_playerObj.transform.position.x-5,_playerObj.transform.position.y+2,_playerObj.transform.position.z);
	_OneUp = Instantiate(OneUp,_pos,Quaternion.identity);
	_OneUp.transform.rotation = Quaternion.Euler(0,180,0);
	var flash: flashBehaviour =  _OneUp.GetComponent(flashBehaviour);
	flash.startFlashing(1);
	Destroy(_OneUp,2);	
	var yPosIncrementer : float = 0;
	while (!gameOver) 
	{		
		yield;	
		if(Time.timeScale!=0)	
		if (!movingDown && !(playerControl.playerDead && player1Control.playerDead)  && levelInProgress) 
		{
			if(_OneUp)
			{		
				_OneUp.transform.position = Vector3(_playerObj.transform.position.x,(_playerObj.transform.position.y+5)+yPosIncrementer,-2);
				yPosIncrementer = yPosIncrementer + 0.08;
			}
			else
				break;
		}
	}
}



function FlashScore(_obj:GameObject)   // Flashing text color yellow to white
{
	var isYellow:boolean = true;
	var startTimer = Time.time +1;
	_obj.audio.Play();
	while((startTimer - Time.time)>=0)
	{
		yield WaitForSeconds(0.15);
		_obj.renderer.material.color = isYellow ? Color.yellow :Color.white ;	
		isYellow = !isYellow;
	}	
	_obj.renderer.material.color = Color.white ;	
}


function ChangePlayerLifeIconTexture(_var : String)
{
	var _scoreObject:GameObject = (_var=="score") ? GameObject.Find("player1Icon") : GameObject.Find("player2Icon") ;
	var _crabAnimatedTextureUV :CrabAnimatedTextureUV = _scoreObject.GetComponent("CrabAnimatedTextureUV") as CrabAnimatedTextureUV;
	_scoreObject.renderer.material.mainTexture =(_var=="score") ?  Resources.Load("PlayerLifeIcon/CrabShoot_L") :Resources.Load("PlayerLifeIcon/YLWCrabShoot_L") ;
	_crabAnimatedTextureUV.changeAnimation(4,1,14,true,0);
	while(1)
	{
		yield;
		if(_crabAnimatedTextureUV.isPlayOnceFinished)
		{
			_scoreObject.renderer.material.mainTexture = (_var=="score") ? Resources.Load("PlayerLifeIcon/crabidle_redL"):Resources.Load("PlayerLifeIcon/crabidle_yellowL");
			_crabAnimatedTextureUV.changeAnimation(5,1,5,false,0);
			break;
		}
	}	
	
}

function AddScore (points : int, source : String) {
	if(source.Equals("init"))
	{
	score += points;
	score1 += points;
	}
	if(gameMode == 0)
	{
	score += points;	
	}
	else if(gameMode == 2)
	{
	score += points;	
	}
	else if(gameMode == 1)
	{
	 	if(source.Equals("player1missile"))
		{
			score += points;
		}
		else if(source.Equals("player2missile"))
		{
			score1 += points;
		}
	}
	
	ExtraLife();
}





function Update()
{
//	Debug.Log("gameMode ="+gameMode+" extraLives ="+extraLives +"  extraLives1="+extraLives1);
	if(gameMode == 0)
	{		
		scoreObject.text = "x" + (extraLives + 1)  + " " +  Utility.getFormatedNumber(score);	
		score1Object.text = " " ;
	}

	if(gameMode == 1)
	{
		scoreObject.text = "x" + (extraLives + 1) + " " +  Utility.getFormatedNumber(score);	
		score1Object.text = Utility.getFormatedNumber(score1) + " " + (extraLives1 + 1) + "x" ;			
	}	
	
}



function InvaderDestroyed (alienType : int, explosionPosition : Vector3, source : String ,isPowerUp:boolean ) 
{
	AddScore(alienPoints[alienType],source);
	if(isPowerUp){
		Instantiate(alienExplosionBySharkPowerUp, explosionPosition, Quaternion.identity);
	}else{
		if(Random.value > .5)
			Instantiate(alienExplosion, explosionPosition, Quaternion.identity);// Most of the time, just blow up the alien...
		else
			Instantiate(alienExplosion1, explosionPosition, Quaternion.identity);// Most of the time, just blow up the alien...	
	}

	
	// Speed up when the number of invaders has been reduced enough, and increase the number of bombs the first time this happens
	if (--numberLeft <= speedupNumber) {
		if (firstSpeedup) {
			twoBombs = true;	
			firstSpeedup = false;
		}
		speedupNumber = numberLeft * .6;
		animateInterval *= .66;
		iSpeed *= speedIncrease;
	}
	
	BuildInvaderArray();	// One less alien, so the array used for bombing has to be rebuilt
	// Start next level if all invaders are destroyed
	if (numberLeft == 0) {
		NextLevel();
	}
	
}


function SetGTButtonStatus(_val:boolean)
{
	var _gtCtrl :GTHomeController = GameObject.Find("GTHome").GetComponent("GTHomeController") as GTHomeController;
	_gtCtrl.enabled = _val;
	
}

function NextLevel () {
	if (gameOver) {return;}	// Just in case the player has lost all lives just as he destroyed the last invader
	
	while(1)   //Debug.Log("wait until bomb clear");
	{
		yield;
		if(!GameObject.FindWithTag("bomb"))		{	
			yield WaitForSeconds(1);
			break;	
		}
	}
	
	SetGTButtonStatus(false);
	
	while (!levelInProgress) {	// If player got blown up just prior to clearing the board, wait
		yield ;
	}
	
	levelInProgress = false;
	levelEnded = true;
	
	
	if(GameObject.FindWithTag("powerUpIcon"))
		Destroy(GameObject.FindWithTag("powerUpIcon"));
	
		
	// Turn off player and get rid of the ufo, if there is one
	DestroyPowerUp_UFO();	
		
	var _powerUpCtrl :powerUpControl = GameObject.Find("_Manager").GetComponent("powerUpControl");
	_powerUpCtrl.DestroyPowerUp();	
	
	
	// Make references to old bunkers and materials, since we'll have new bunkers and the old ones at the same time briefly
	// And we don't want to overwrite the old bunkers with new ones just yet
	var oldBunkers = new GameObject[bunkers.length];
	var oldBunkerMaterials = new Material[bunkers.length];
	for (var i = 0; i < bunkers.length; i++) {
		if (bunkers[i]) {	// These can potentially be destroyed if invaders touch them, so we have to check to see if they exist
			oldBunkers[i] = bunkers[i];			
		}
		oldBunkerMaterials[i] = bunkerMaterials[i];
	}
	
	// Set up next level some ways ahead out of sight...we will zoom to it soon
	startHeight = Mathf.Max(--startHeight, 2);	// No lower than 2
		//player.renderer.enabled = false;
	//if(gameMode != 0) player1.renderer.enabled = false;
	SetUpInvaders(1000.0);
	//yield WaitForSeconds(1.5);

	// Accelerate forward until objects from current level are behind us
	cam.constantForce.enabled = true;
	cam.constantForce.force.z = 140.0;
	
	levelSwoosh.Play();
	
	
	yield WaitForSeconds(.5);
	
	while (cam.transform.position.z < 100.0) {
		yield;
	}

	// Destroy old bunkers and their materials
	for (i = 0; i < bunkers.length; i++) {
		if (oldBunkers[i]) {
			Destroy(oldBunkers[i]);
		}
		Destroy(oldBunkerMaterials[i]);
	}

	level++;
	InitializeLevel(1000.0);
	powerUp_probability_chance = 1;
		
	// The ground meshes are 320 units big and they're all the same, so we just go back exactly twice that distance and you can't tell
	// More convenient than actually trying to make an endless terrain
	cam.transform.position.z -= 180.0;
	
	// Wait until we reach the halfway point and then slow down by applying negative force
	while (cam.transform.position.z < -120.0) {
		yield;
	}
	cam.constantForce.force.z = -139.0;	// Use a bit less negative force than the original positive force...
										// there's small chance of going backwards otherwise if it doesn't work out exactly
	// Wait until we reach the final desired position and then stop
	while (cam.transform.position.z < -60.0) {
		yield ;
	}
	SetGTButtonStatus(true);
	cam.transform.position.z = -60;
	cam.constantForce.force.z = 0.0;
	cam.rigidbody.velocity.z = 0.0;
	cam.constantForce.enabled = false;
	twoBombs = false;
}

function SetPlayerCollisionStatus(_val:boolean,_gameObject:GameObject)
{
			
	if(_val)
	{
		_gameObject.transform.position.z = 5.0;		
	}else
	{
		yield WaitForSeconds(0.5f);
		_gameObject.transform.position.z = 0;
	}	
}


function BaseExplode (source : String) {

	CameraShake();
	var _powerUpCtrl :powerUpControl = GameObject.Find("_Manager").GetComponent("powerUpControl");
	_powerUpCtrl.DeactivePowerUp();
	if(source.Equals("player1"))
	{
		
		var flashing1: flashBehaviour;
		flashing1 = player.GetComponent("flashBehaviour");
		if(flashing1.isFlashing) return;
		
		SetPlayerCollisionStatus(true,player);
		player.GetComponent(PlayerControl).Explode();
		playerControl.playerCanMove = false;
		playerControl.playerDead = true;		
		player.renderer.enabled = false;	// There's not much to be gained here by instantiating/destroying the player, so just enabled/disable instead
		player.transform.position -= Vector3.forward*10.0; // Move player back so there's no chance of getting hit by another bomb
		extraLives--;
		if (extraLives >= 0) {
			player.transform.position += Vector3.forward*10.0;
			levelInProgress = true;
			if (!levelEnded) {	// Don't start player if he got blown up just as a level ended...InitializeLevel will do that instead
				yield WaitForSeconds(0.2f);
				StartPlayerNo(1);
				BombControl(.7);
			}
		}
		
		(player.GetComponent("PlayerTextureManager") as PlayerTextureManager).TurnCrabColors(Color.white);
	}
	if(source.Equals("player2"))
	{	
		var flashing2: flashBehaviour;
		flashing2 = player1.GetComponent("flashBehaviour");
		if(flashing2.isFlashing) return;
		SetPlayerCollisionStatus(true,player1);
		player1.GetComponent(PlayerControl).Explode();
		player1Control.playerCanMove = false;
		player1Control.playerDead = true;	
		player1.renderer.enabled = false;	// There's not much to be gained here by instantiating/destroying the player, so just enabled/disable instead
		player1.transform.position -= Vector3.forward*20.0; // Move player back so there's no chance of getting hit by another bomb
		extraLives1--;		
		if (extraLives1 >= 0) {		
			player1.transform.position += Vector3.forward*20.0;
			levelInProgress = true;
			if (!levelEnded) {	// Don't start player if he got blown up just as a level ended...InitializeLevel will do that instead
				yield WaitForSeconds(0.2f);
				StartPlayerNo(2);
				BombControl(.7);
			}
		}
		(player1.GetComponent("PlayerTextureManager") as PlayerTextureManager) .TurnCrabColors(Color.white);
	}
	
	

	//yield WaitForSeconds(3.0);
	
	// Start back up again if the player still has any lives left
	if((extraLives < 0) && (extraLives1 <0)) {
		gameOver = true;
		levelInProgress = false;
	}
}

var startingShakeDistance = .8;
var decreasePercentage = .5;
var shakeSpeed = 55.0;
var numberOfShakes = 3;

function CameraShake () {
	var hitTime = Time.time;
	var originalPosition = cam.transform.localPosition.x;
	var shakeCounter = numberOfShakes;
	var shakeDistance = startingShakeDistance;

	while (shakeCounter > 0) {
		// Make timers always start at 0 
		var timer = (Time.time - hitTime) * shakeSpeed;
		cam.transform.localPosition.x = originalPosition + Mathf.Sin(timer) * shakeDistance;
		// See if we've gone through an entire sine wave cycle, reset distance timer if so and do less distance next cycle
		if (timer > Mathf.PI * 2.0) {
			hitTime = Time.time;
			shakeDistance *= decreasePercentage;
			shakeCounter--;
		}
		yield;
	}
	cam.transform.localPosition.x = originalPosition;
}

