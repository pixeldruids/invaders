static var instance : GameManager;

var loadingTex : GameObject;
var barOut : GameObject;
var barIn : GameObject;
var barBlack : Transform;
var stars : GameObject;

var thisGameName : String;
var testing = false;
var homeURL = "";


function Awake() {
	instance = this;
	DontDestroyOnLoad (this);
	InputManager.Instance.greenThrottle.Reset();
}

function LoadingBar() {
	// Turn stars on and advance particle system by 6 seconds (but not in real time) so they appear already in place
	// Unfortunately ParticleEmitter.Simulate() doesn't generate the desired results
	stars.active = true;
	Time.timeScale = 99.0;
	yield WaitForSeconds(6);
	Time.timeScale = 1.0;
	stars.renderer.enabled = loadingTex.active = barOut.active = barIn.active = barBlack.gameObject.active = true;

	// Reveal loading bar as the level is streamed
	while (!Application.CanStreamedLevelBeLoaded("Menu")) {
		var p = Application.GetStreamProgressForLevel("Menu");
		barBlack.position.x = Mathf.Lerp(.25, .75, p);
		barBlack.localScale.x = Mathf.Lerp(1.0, 0.0, p);
		yield;
	}
	
	Application.LoadLevel("Menu");
}

