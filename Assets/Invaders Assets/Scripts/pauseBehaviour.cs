using UnityEngine;
using System.Collections;

public class pauseBehaviour : MonoBehaviour {
	
	int currentSelected;
	public buttonBehaviour []buttonList;
//	public InvaderControl invaderCtrl;
	
	void Start () {				
		currentSelected = 0;
		InputManager.Instance.greenThrottle.Reset();
		this.gameObject.SetActiveRecursively(false);
		//bGamePaused = false;
	}
	
	public void Show()
	{
		this.gameObject.SetActiveRecursively(true);
	}
	public void Hide()
	{
		this.gameObject.SetActiveRecursively(false);
	}

	// Update is called once per frame
	void Update () {
		
		if(Input.GetKeyDown(KeyCode.LeftArrow) || InputManager.Instance.GetKeyDown(InputManager.IB_LEFT))
		{
			++currentSelected;			
		}
		if(Input.GetKeyDown(KeyCode.RightArrow) || InputManager.Instance.GetKeyDown(InputManager.IB_RIGHT))
		{
			--currentSelected;
		}
		if(currentSelected > 2) currentSelected = 0;
		if(currentSelected < 0) currentSelected = 2;
		
		if(Input.GetKeyDown(KeyCode.Space) || InputManager.Instance.GetKey(InputManager.IB_FIRE))
		{			
			makeDecisionOnSelection();
		}				
		//bGamePaused = this.gameObject.active;
		
	}
	void makeDecisionOnSelection()
	{
		switch(currentSelected)
		{
			case 0: {							
				Application.LoadLevel("Menu");
				break;
			}			
			case 1: {							
				Application.LoadLevel("MainGame");
				break;
			}			
			case 2: {			
				Hide ();
				break;
			}
		}

	}
	
}
