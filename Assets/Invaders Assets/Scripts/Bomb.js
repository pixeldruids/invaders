// Controls behavior of bombs dropped by invaders

var bombExplosion : GameObject;
var bubbleShieldBur : BubbleShieldBehaviour;
var bombExplosionOnBunkers: GameObject;


function OnCollisionEnter (other : Collision) {


	if (other.collider.tag == "Player") {	
		InvaderControl.use.BaseExplode(other.gameObject.name);
		Explode();
	}
	else if (other.collider.name == "Ground") {
		Explode();
	}
	else{
			Destroy(gameObject);
	}
}
function OnTriggerEnter (other : Collider) 
{

	if(other.gameObject.tag.ToLower().Contains("bubble"))
	{
		bubbleShieldBur = GameObject.FindWithTag("bubble").GetComponent("BubbleShieldBehaviour");
		bubbleShieldBur.DestroyBubble();	
		Destroy(gameObject);
	}
	
}


function Explode() {	// Also called from BunkerBehavior
	Instantiate(bombExplosion, transform.position - Vector3.forward*1.5, Quaternion.identity);
	Destroy(gameObject);
}

function ExplodeOnBunkers() {
	Instantiate(bombExplosionOnBunkers, transform.position - Vector3.forward*1.5, Quaternion.identity);
	Destroy(gameObject);
}