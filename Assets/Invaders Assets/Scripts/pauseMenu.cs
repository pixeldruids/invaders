using UnityEngine;
using System.Collections;

public class pauseMenu : MonoBehaviour {
	
	int currentSelected;
	GameObject homeBtn;
	GameObject restartBtn;	
	GameObject resumeBtn;
	//bool bGamePaused;
	ArrayList HoverList = new ArrayList();
	ArrayList NormalList = new ArrayList();
	InputManager inputManager;
	
	
	void Start () {				
		currentSelected = 0;
		homeBtn =  GameObject.FindWithTag("homeBtn");
		restartBtn =  GameObject.FindWithTag("restartBtn");
		resumeBtn =  GameObject.FindWithTag("resumeBtn");
		
		HoverList.Add((Texture)Resources.Load("Menu/P_Home_r")); 
		HoverList.Add((Texture)Resources.Load("Menu/P_Restart_r"));	
		HoverList.Add((Texture)Resources.Load("Menu/P_Play_r")); 
		NormalList.Add((Texture)Resources.Load("Menu/P_Home"));		
		NormalList.Add((Texture)Resources.Load("Menu/P_Restart"));
		NormalList.Add((Texture)Resources.Load("Menu/P_Play"));		
		inputManager = (InputManager)GameObject.FindWithTag("inputmanager").GetComponent("InputManager");	
		inputManager.greenThrottle.Reset();
		this.gameObject.SetActiveRecursively(false);
		//bGamePaused = false;
	}
	
	public void Show()
	{
		this.gameObject.SetActiveRecursively(true);
	}
	public void Hide()
	{
		this.gameObject.SetActiveRecursively(false);
	}

	// Update is called once per frame
	void Update () {
		
		if(Input.GetKeyDown(KeyCode.LeftArrow) || inputManager.GetKeyDown(InputManager.IB_LEFT))
		{
			++currentSelected;			
		}
		if(Input.GetKeyDown(KeyCode.RightArrow) || inputManager.GetKeyDown(InputManager.IB_RIGHT))
		{
			--currentSelected;
		}
		if(currentSelected > 2) currentSelected = 0;
		if(currentSelected < 0) currentSelected = 2;
		AssignTextures();
		if(Input.GetKeyDown(KeyCode.Space) || inputManager.GetKey(InputManager.IB_FIRE))
		{			
			makeDecisionOnSelection();
		}				
		//bGamePaused = this.gameObject.active;
		
	}
	void makeDecisionOnSelection()
	{
		switch(currentSelected)
		{
			case 0: {							
				Application.LoadLevel("Menu");
				break;
			}			
			case 1: {		
				InputManager.Instance.greenThrottle.Reset();	
				Application.LoadLevel("MainGame");
				break;
			}			
			case 2: {			
				Hide ();
				break;
			}
		}

	}
	
	public void AssignTextures()
	{
		if(currentSelected == 0) 
		{
			homeBtn.guiTexture.texture = (Texture)HoverList[0];
			restartBtn.guiTexture.texture = (Texture)NormalList[1];
			resumeBtn.guiTexture.texture = (Texture)NormalList[2];			
			return;
		}
		if(currentSelected == 1) 
		{
			homeBtn.guiTexture.texture = (Texture)NormalList[0];
			restartBtn.guiTexture.texture = (Texture)HoverList[1];
			resumeBtn.guiTexture.texture = (Texture)NormalList[2];			
			return;
		}
		if(currentSelected == 2) 
		{			
			homeBtn.guiTexture.texture = (Texture)NormalList[0];
			restartBtn.guiTexture.texture = (Texture)NormalList[1];
			resumeBtn.guiTexture.texture = (Texture)HoverList[2];
			return;
		}

	}
}
