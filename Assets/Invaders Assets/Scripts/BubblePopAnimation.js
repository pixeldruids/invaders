
var uvAnimationTileX :int ; //Here you can place the number of columns of your sheet. 
                           //The above sheet has 24

var uvAnimationTileY :int; //Here you can place the number of rows of your sheet. 
                          //The above sheet has 1
var framesPerSecond :float ;

var playOnce : boolean ;

var isPlayOnceFinished : boolean;

 var index : int = 0 ;
 
function Start()
{
	
} 
 
function updateTiming()
{
	index++;
}


function SetFields(_x:int,_y:int,_frame:int,_val:boolean)
{
	uvAnimationTileX = _x;
	uvAnimationTileY = _y;
	framesPerSecond = _frame;
	playOnce = _val;
	index = 0;
	InvokeRepeating("updateTiming",0.1f,0.1f);
}

function Update () {    
    // repeat when exhausting all frames
//    if(uvAnimationTileX == 0) return;
 //   if(uvAnimationTileY == 0) return;
    index = index % (uvAnimationTileX * uvAnimationTileY);
   
    // Size of every tile
    var size = Vector2 (1.0 / uvAnimationTileX, 1.0 / uvAnimationTileY);
    
    var offset :Vector2 ;
   
    if(playOnce)
    {
    	if(!isPlayOnceFinished)
    	{
    		// split into horizontal and vertical index
    		var uIndex = index % uvAnimationTileX;
    		var vIndex = index / uvAnimationTileX;
    		 if(uIndex == (uvAnimationTileX-1)){
    		 	uIndex =  (uvAnimationTileX-1);
    		 	isPlayOnceFinished = true;
    		 	CancelInvoke("updateTiming");
    		 	isPlayOnceFinished = true;
    		 	}
    	}
    	else
    	{
    		Destroy(gameObject);
    	}
    }
    else
    {
    	// split into horizontal and vertical index
    	 uIndex = index % uvAnimationTileX;
    	 vIndex = index / uvAnimationTileX;	
    }
    
    offset = Vector2 (uIndex * size.x, 1.0 - size.y - vIndex * size.y);
    
    
    renderer.material.SetTextureOffset ("_MainTex", offset);
    renderer.material.SetTextureScale ("_MainTex", size);
}