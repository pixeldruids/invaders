#pragma strict

var currentSelected:int;
var invaderCtrl:InvaderControl;
var ButtonList:buttonBehaviour[] = new buttonBehaviour[3];

var menuSelectionSound:AudioClip;
var menuEnterSound:AudioClip;

var bPaused:boolean;
var isDelay : boolean = true;

function Start () {
	
		currentSelected = 0;
		InputManager.Instance.greenThrottle.Reset();
		invaderCtrl = GameObject.Find("_Manager").GetComponent("InvaderControl") as InvaderControl;	
		bPaused = false;	
		Invoke("DelayUpdate",0.5f);
}


function DelayUpdate()
{
	isDelay = true;
}

function PlayMenuSelectionSound()
	{
		audio.clip = menuSelectionSound;
		audio.Play();
	}
	
	function PlayMenuEnterSound()
	{
		audio.clip = menuEnterSound;
		audio.Play();
	}
	
function Update () {
	
	if(!isDelay)
		return;
			
	if(bPaused)
	{
		if(Input.GetKeyDown(KeyCode.Escape) || InputManager.Instance.GetKeyDown(InputManager.IB_SELECT))
		{
			Time.timeScale = 1; 
			Application.LoadLevel("Menu");
		}
		
		for(var i = 0; i < ButtonList.Length; i++)
		{
			if(currentSelected == i) ButtonList[i].setSelected();
			else ButtonList[i].setNotSelected();
		}

		this.transform.position.y = 0.9f;
		if(Input.GetKeyDown(KeyCode.DownArrow) || InputManager.Instance.GetKeyDown(InputManager.Instance.IB_BACK))
		{
			PlayMenuSelectionSound();
			++currentSelected;			
		}
		if(Input.GetKeyDown(KeyCode.UpArrow) || InputManager.Instance.GetKeyDown(InputManager.Instance.IB_FORWARD))
		{
			PlayMenuSelectionSound();
			--currentSelected;
		}
		if(currentSelected > 2) currentSelected = 0;
		if(currentSelected < 0) currentSelected = 2;
		
		if(Input.GetKeyDown(KeyCode.Space) || InputManager.Instance.GetKeyDown(InputManager.Instance.IB_FIRE))
		{			
			makeDecisionOnSelection();
		}		
		
		 if(InputManager.Instance.GetKeyDown(InputManager.IB_START))
		 {
		 	currentSelected = 0;
		 	makeDecisionOnSelection();
		 }
	}	
	else
	{
	this.transform.position.y = 50;
	}	

}
function Show()
{
	if(invaderCtrl.gameOver)
		return;
	bPaused = true;
	invaderCtrl.PauseGame();
	invaderCtrl.SetUfoSoundStatus(true);
	this.gameObject.SetActiveRecursively(true);
}
function Hide()
{
	//Camera.mainCamera.transform.localPosition -= Vector3(0,0,60);
	InputManager.Instance.greenThrottle.Reset();
	bPaused = false;
	invaderCtrl.PauseGame();
	Utility.SetVolumeForPauseScreen(1);
	invaderCtrl.SetUfoSoundStatus(false);
	this.gameObject.SetActiveRecursively(false);
}

function makeDecisionOnSelection()
{

	Time.timeScale = 1;  // activate timer 
	if(currentSelected == 0)
	{
		PlayMenuEnterSound();
		yield WaitForSeconds(0.2);
		Time.timeScale = 0;
		Hide();
	}
	if(currentSelected == 1)
	{
		PlayMenuEnterSound();
		yield WaitForSeconds(0.2);
		Application.LoadLevel("Menu");
		
	}
	if(currentSelected == 2)
	{
		PlayMenuEnterSound();
		yield WaitForSeconds(0.2);
		Application.LoadLevel("MainGame");
	}

}
	
