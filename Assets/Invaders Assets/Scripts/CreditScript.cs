using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class CreditScript : MonoBehaviour {

	public GameObject contentsAlignmentRight ;
	public GameObject contentsAlignmentLeft ;
	public GameObject contentsAlignmentCenter ;
	public GameObject gameTitle;
	public GameObject groupHeader;
	private Vector3 Pos = new Vector3(10,-10,-13);//new Vector3(10,90,10);
	public AudioClip menuBackSound;
	
	// Use this for initialization
	void Start () {
			InputManager.Instance.greenThrottle.Reset();
			Credits();
			Invoke("MoveIn",1);
	}
	
	void MoveIn()
	{
		iTween.MoveTo(gameObject,iTween.Hash("y",733,"time", 55f,"onComplete","returnToSettings"));	
	}
	
	void returnToSettings()
	{
		Invoke("MakeDelaySettings",7);
	}
	
	void MakeDelaySettings()
	{
		Application.LoadLevel("Settings");
	}
	
	void PlayMenuBackSound()
	{
		audio.clip = menuBackSound;
		audio.Play();
	} 
	
	// Update is called once per frame
	void Update () {
	
		if(Input.GetKeyDown(KeyCode.Escape) || InputManager.Instance.GetKey(InputManager.IB_SELECT))
		{			
			PlayMenuBackSound();
			Application.Quit();
		}	
		
		if(Input.GetKeyDown(KeyCode.Escape) || InputManager.Instance.GetKeyDown(InputManager.IB_PREV))
		{
			PlayMenuBackSound();
			Application.LoadLevel("Settings");
			
		}
	}
	
	void MakeChildren(GameObject _obj)
	{
		if(gameObject)
		{
			_obj.transform.parent = gameObject.transform;	
		}
	}
	
	void GameTitle()
	{
		GameObject _title = Instantiate(gameTitle,Pos,Quaternion.identity) as GameObject;
		MakeChildren(_title);
	}
	
	void GroupHeader(string _header,Vector3 _pos)
	{
		GameObject contentObject ;		
		contentObject = Instantiate(groupHeader,_pos,Quaternion.identity) as GameObject;
		(contentObject.GetComponent("TextMesh") as TextMesh).text =_header;
		MakeChildren(contentObject);
		SetEndPosition(_pos);
	}
	
	void Credits()
	{
		GameTitle();
		Pos.y-=20;
		/************* iNEXGENGAMES  CREDITS*************/
		
		GroupHeader("iNexGen Games",Pos);
		Pos.y-=15;
		string[,] inexgenArray = new string[,]
		{
			{"0","Development Head"},
			{"1","Swami Venkat"},
			{"0","Programming Manager"},
			{"1","Ramkumar Kolpurath"},
			{"0","Technical Lead"},
			{"1","Sundara Pandian"},
			{"0","Game Developer"},
			{"1","Srinivasan T V"}
		};
		Vector3 contentStartPos =Pos;
		DisplayContent(inexgenArray,contentStartPos);
		/**************** END *********************/
		
		/************ GT Production **************/
		Pos.y-=10;Pos.x -=10;
		GroupHeader("Green Throttle Games",Pos);
		Pos.x +=10;
		Pos.y-=15;Pos.x -=10;
		GroupHeader("Production",Pos);
		Pos.y-=15;Pos.x +=10;
		string[,] productionArray = new string[,]
		{
			{"0","Executive Producer"},
			{"1","Jason Woodward"},
			{"0","Producer"},
			{"1","Tiffany Nelson"},
			{"0","Game Design"},
			{"1","Erik Yeo"},
			{"0","Programming"},
			{"1","Joseph Filomena"},
			{"1","Daniel Tomalesky"},
			{"0","Quality Assurance"},
			{"1","Chris Larkin"},
			{"1","RTL Labs"}
		};
		contentStartPos =new Vector3(Pos.x,Pos.y,Pos.z);
		DisplayContent(productionArray,contentStartPos);
		/**************** END ********************/
		
		Pos.y-=10;
		Pos.x -=20;
		GroupHeader("Art & Animation",Pos);
		Pos.y-=15;
		Pos.x +=20;
		
		/************* Art & Animation ************/
		string[,] ArtAnimArray = new string[,]
		{
			{"0","Creative Director"},
			{"1","Michael Doan"},
			{"0","Concepts"},
			{"1","Ishmael Hoover"},
			{"0","Animation"},
			{"0","Characters & Objects"},
			{"0","User Interface"},
			{"0","Game Logo"}
			
		};
		contentStartPos =new Vector3(Pos.x,Pos.y,Pos.z);
		DisplayContent(ArtAnimArray,contentStartPos);
		/**************** END ************************/
		Pos.y-=15;
		Pos.x +=15;
		GroupHeader("Audio",Pos);
		Pos.y-=15;
		Pos.x -=15;
		/*************** Audio ****************/
		string[,] AudioArray = new string[,]
		{
			{"0","Sound Design"},
			{"1","Erik Yeo"},	
		};
		contentStartPos =new Vector3(Pos.x,Pos.y,Pos.z);
		DisplayContent(AudioArray,contentStartPos);
		
		string[] _AudioArray = new string[] 
		{ 
			"\"Little Robots Calypso 3\"","By Philip Guyler","2012","Published by Audio Network Plc"			
		};
		Pos.y+=10;
		contentStartPos =new Vector3(Pos.x-15,Pos.y-10,Pos.z);
		DisplayCenterAlignmentContent(_AudioArray,contentStartPos);
		
		/*************** END *******************/
			
		Pos.y-=10;
		GroupHeader("Marketing",Pos);
		Pos.y-=15;
		
		/*************Marketing************/
		string[,] MarketingArray = new string[,]
		{
			{"0","Brand Manager"},
			{"1","Megan Withers"},
			{"0","Community Relations"},
			{"1","Kyle Rechsteiner"}	
		};
		contentStartPos =new Vector3(Pos.x,Pos.y,Pos.z);
		DisplayContent(MarketingArray,contentStartPos);
		/**************** END ************************/
		Pos.y-=10;
		Pos.x -=15;
		GroupHeader("Management & Staff",Pos);
		Pos.x +=15;
		Pos.y-=15;
		/*************Management & Staff************/
		string[,] ManagementStaffArray = new string[,]
		{
			{"0","CEO"},
			{"1","Charles Huang"},
			{"0","President & COO"},
			{"1","Matt Crowley"},
			{"0","CTO"},
			{"1","Karl Townsend"},
			{"0","Head of Publishing"},
			{"1","Stacey Hirata"},
			{"0","Human Resources"},
			{"1","Pam Beyazit"},
			{"0","Finance"},
			{"1","Tina Xu"}
		};
		contentStartPos =new Vector3(Pos.x,Pos.y,Pos.z);
		DisplayContent(ManagementStaffArray,contentStartPos);
		/**************** END ************************/
			
		Pos.y-=10;
		Pos.x -=20;
		GroupHeader("Special Thanks",Pos);
		Pos.y-=15;
		Pos.x +=20;
		
		
		/*********** Special Thanks  *************/
		string[] SpecialThanksArray = new string[] 
		{ 
			"Kai Huang","Campbell Crowley","Emma Crowley","Mary Dougherty","Outerfere","Raja Software LLC","Unity Technologies ApS	","All the friends and family who have supported us at Green Throttle"			
		};
		contentStartPos =new Vector3(Pos.x-15,Pos.y,Pos.z);
		DisplayCenterAlignmentContent(SpecialThanksArray,contentStartPos);
		
		/************* END ****************/
		
		/*********** Thanks  *************/
		string[] ThanksArray = new string[] 
		{ 
			"Thanks for playing!","Discover more games at GreenThrottle.com"
		};
		Pos.y -=100;
		contentStartPos =new Vector3(Pos.x,Pos.y,Pos.z);
		DisplayCenterAlignmentContent(ThanksArray,contentStartPos);
		
		/************* END ****************/
		
	}
	
	void DisplayCenterAlignmentContent(string[] _contentArray,Vector3 _pos)
	{
		for(int i =0 ; i< (_contentArray.Length);i++)
		{
			GameObject contentObject ;		
			contentObject = Instantiate(contentsAlignmentCenter,_pos,Quaternion.identity) as GameObject;
			(contentObject.GetComponent("TextMesh") as TextMesh).text =_contentArray[i];
			MakeChildren(contentObject);
			_pos.y -= 10;
		}
		SetEndPosition(_pos);
	}
	
	void DisplayContent(string[,] _contentArray,Vector3 _pos)
	{
		string previousIndex ="";
		
		for(int i =0 ; i< (_contentArray.Length/2);i++)
		{
			int j=0;
			GameObject contentObject ;
			
			if(_contentArray[i,j] == "1") 
			{
				_pos.x = 15;
				_pos.y = (_pos.y +5);
				contentObject = Instantiate(contentsAlignmentLeft,_pos,Quaternion.identity) as GameObject;
				MakeChildren(contentObject);
				_pos.y = (_pos.y -5);
				_pos.y -= 10;
			}
			else{
				if(previousIndex == _contentArray[i,j])
					_pos.y -= 10;
				
				_pos.x = -15;
				contentObject =Instantiate(contentsAlignmentRight,_pos,Quaternion.identity) as GameObject;
				MakeChildren(contentObject);
			}
			
			(contentObject.GetComponent("TextMesh") as TextMesh).text =_contentArray[i,(j+1)];	
			
			previousIndex = _contentArray[i,j];
		}
		
		SetEndPosition(_pos);
	}
	
	
	void SetEndPosition(Vector3 _pos)
	{
		Pos = _pos;	
	}

}
