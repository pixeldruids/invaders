// Moves player base and launches missiles
#pragma strict
var moveSpeed = 15.0;
var leftBoundary = 0.0;
var rightBoundary = 10.0;

var missile : Rigidbody;
var missile_FreezePowerUp : Rigidbody;
var missileSpeed = 130.0;
var explosion : GameObject;

private var missileStart:boolean = false;


private var axisH: float = 0;

var btnStyle:GUIStyle;
var pauseObj:GameObject;
var pauseMenu:pauseJS;

var xValue:int;

var preXValue :int = 2;

var bEnableController1:boolean;

var flashPlayer:flashBehaviour;


var playerCanMove : boolean;
var playerDead : boolean;
var isCrabbageddon:boolean;
var playerNo:int;
var missileName : String;

function Start ()
 {

	missileName = "player1";
	missileStart = false;
	btnStyle.normal.background = null;	
	InputManager.Instance.greenThrottle.Reset();
	moveSpeed = moveSpeed/2;
	bEnableController1 = true;
	flashPlayer = gameObject.GetComponent("flashBehaviour") as flashBehaviour;
	playerCanMove = true;
	playerDead = false;	
	isCrabbageddon = false;
	SetplayerMoveSpeed(false);
}	

function Awake()
{
	pauseObj = GameObject.FindWithTag("pauseGUI");
	pauseMenu = pauseObj.GetComponent("pauseJS") as pauseJS;	
}

function SetplayerMoveSpeed( _val )
{
	if(_val)
	{
			moveSpeed = 30.0;
	}
	else
	{
			moveSpeed = 15.0;
	}
}


function enableController1()
{
	bEnableController1 = true;
}

function Update () 
{
		if(Input.GetKeyDown(KeyCode.Escape) || InputManager.Instance.GetKey(InputManager.IB_SELECT))
		{
			Application.LoadLevel("Menu");
		}
	
		if( (GameObject.Find("_Manager").GetComponent("InvaderControl") as InvaderControl).isLevelEnded())
			return;
			
		if (Input.GetButtonDown("Pause") || InputManager.Instance.GetKeyDown(InputManager.IB_START)  ) 
		{	
			if(!pauseMenu.bPaused)
			{
				pauseMenu.Show();
			}
		}
		
		if(! gameObject.renderer.enabled){
			return;
		}
		
		if(pauseMenu.bPaused)
			return;
		
		if(!gameObject)
			return;
				
		missileName = "player1";	
		playerNo = InputManager.Instance.greenThrottle.CONTROLLER_1;
		
		if(this.gameObject.name == "player2")
		{
			playerNo = InputManager.Instance.greenThrottle.CONTROLLER_2;
			missileName = "player2";
		}
		
	// Move player's base back and forth and launch missiles
		if (playerCanMove) 
		{
			#if UNITY_ANDROID
			xValue = InputManager.Instance.GetAxis(InputManager.IA_X_ANALOG,playerNo + 1);	
			transform.position.x = Mathf.Clamp(
									(transform.position.x - xValue  * Time.deltaTime * moveSpeed),leftBoundary,rightBoundary);
			#else
			transform.position.x = Mathf.Clamp(transform.position.x +  GetHorizontalMovement(),leftBoundary,rightBoundary);
			transform.position.x = Mathf.Clamp(transform.position.x +  GetHorizontalKB("player"),leftBoundary,rightBoundary);
			#endif						
			// Can only fire one missile at a time, and also only when a level isn't ended
			if (InputManager.Instance.GetKey(InputManager.IB_R2, playerNo + 1)||(InputManager.Instance.GetKey(InputManager.IB_FIRE, playerNo + 1) || Input.GetButton("Fire1"))/* && !missileInFlight */&& !InvaderControl.levelEnded) 
			{
			
				if(bEnableController1)
				{
					bEnableController1 = false;
	
					var missileClone1 : Rigidbody ;
					if( isCrabbageddon)
					{
						missileClone1 = Instantiate(missile_FreezePowerUp, transform.position + Vector3.up, Quaternion.identity);
						Invoke("enableController1",0.1f);
					}
					else
					{
						missileClone1 = Instantiate(missile, transform.position + Vector3.up, Quaternion.identity);	
						Invoke("enableController1",0.6f);
						SetplayerMoveSpeed(false);
					}
					missileClone1.velocity.y = missileSpeed;
					missileClone1.name =missileName+"missile";
					(gameObject.GetComponent(PlayerTextureManager) as PlayerTextureManager).SetMissileStatus(true);
					
					if(missileName == "player1")
					{
						if(GameObject.Find("player2missile"))
							Physics.IgnoreCollision (GameObject.Find("player1missile").collider, GameObject.Find("player2missile").collider);	
					}
					else if(missileName == "player2")
					{
						if(GameObject.Find("player1missile"))
							Physics.IgnoreCollision (GameObject.Find("player1missile").collider, GameObject.Find("player2missile").collider);	
					}
					
				}				
			}
		}	
		
			
}


function GetPlayerCanMoveStatus()
{
	return playerCanMove;
}

public function GetCurrentStatusOfJoyStick()
{
	return xValue;
}

public function GetMissileStatus()
{
	return missileStart;
}



function GetHorizontalKB(_type:String) : float
 {
 	if(Input.GetKey(KeyCode.LeftArrow))
	{	
		xValue = 1;
		return -1 * Time.deltaTime * moveSpeed;
	}
	else if(Input.GetKey(KeyCode.RightArrow))
	{
		xValue = -1;
		return 1 * Time.deltaTime * moveSpeed;
	}
	else
		xValue = 0;
	
	return 0;
 }

function GetHorizontalMovement() : float 
{
	return InputGetAxis("DPadHorizontal") * Time.deltaTime * moveSpeed;
}

function InputGetAxis(axis: String): float {

    var v = Input.GetAxis(axis);
    if (Mathf.Abs(v) > 0.005) return v;
    if (axis) return axisH;
    return 0;
}

function Explode () {

	Instantiate(explosion, transform.position, Quaternion.identity);
	flashPlayer.startFlashing(3.0f);
}
