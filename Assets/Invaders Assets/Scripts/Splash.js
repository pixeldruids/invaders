#pragma strict
#pragma implicit
#pragma downcast

private var selectedVideo = "GTLogo.mp4";

function Start () {
	 //Screen.orientation = ScreenOrientation.LandscapeLeft;
	Handheld.PlayFullScreenMovie(selectedVideo, Color.black, FullScreenMovieControlMode.Hidden, FullScreenMovieScalingMode.AspectFit);
    Invoke("LoadNextLevel",0.7);
}

function LoadNextLevel()
{
	Application.LoadLevel("Menu");
}

