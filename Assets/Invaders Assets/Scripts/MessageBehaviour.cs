using UnityEngine;
using System.Collections;

public class MessageBehaviour : MonoBehaviour {
	
	InputManager inputmgr;
	// Use this for initialization
	void Start () {
	inputmgr = (InputManager)GameObject.FindWithTag("InputManager").GetComponent("InputManager");
		inputmgr.greenThrottle.Reset();
	}
	
	// Update is called once per frame
	void Update () {
	this.guiText.text = inputmgr.GetKey(InputManager.IB_FIRE).ToString();
	}
}
