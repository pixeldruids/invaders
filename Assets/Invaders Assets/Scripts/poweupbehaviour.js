#pragma strict

var alienExplosion : GameObject;

function Start () {

}

function Update () {

}

function OnTriggerEnter (other : Collider) 
{
	if( (other.name == "player1") || (other.name == "player2") )
	{
		var flashing1: flashBehaviour;
		flashing1 = other.gameObject.GetComponent("flashBehaviour");
		if(flashing1.isFlashing) return;
		
		var _pos : Vector3= other.transform.position; 
		var _name  = gameObject.name; 
		var explode : GameObject = Instantiate(alienExplosion, _pos, Quaternion.identity);
		explode.transform.position.y += 5;
		EnablePowerUp(_name,other.name);
		Destroy(gameObject);
	}
}


function EnablePowerUp(_name : String,_playerName)
{
	powerUpControl.StringToEnum(_name,_playerName);
}