var normalTexture:Texture2D;
var hoverTexture:Texture2D;

var invaderCtrl:InvaderControl;

var MenuBg:GUIStyle;
var homeBtn:GUIStyle;
var playAgainBtn:GUIStyle;
var resumeBtn:GUIStyle;

var menu_box_width:int = 849;
var menu_box_height:int = 341;
var menu_pos_x:int = 0; 
var menu_pos_y:int = 0;

var enableGUI:boolean = false;

var currentSelected:int = 0;


	
	
function Start()
{
	InputManager.Instance.greenThrottle.Reset();
	
}

function OnMouseDown()
{
	if(gameObject.name == "PauseObject" ){
		if(!enableGUI){
			LoadMenuItems();			
		}
	}
		
}

	
	
function Update()
{

	if(Input.GetKeyDown(KeyCode.UpArrow) || InputManager.Instance.GetKeyDown(InputManager.IB_FORWARD))
	{
		--currentSelected;
	}
	if(Input.GetKeyDown(KeyCode.DownArrow) || InputManager.Instance.GetKeyDown(InputManager.IB_BACK))
	{
		++currentSelected;
	}
	if(currentSelected > 2) currentSelected = 0;
	if(currentSelected < 0) currentSelected = 2;
	if(Input.GetKeyDown(KeyCode.Space) || InputManager.Instance.GetKey(InputManager.IB_FIRE))
	{			
	}				
		

}
function LoadMenuItems()
{
	invaderCtrl = GameObject.Find("_Manager").GetComponent("InvaderControl");
	
	menu_pos_x = (Screen.width/2)-200;
	menu_pos_y = (Screen.height/2)-100;
		
	MenuBg.normal.background = Resources.Load("Menu/Space_Play_menu");
		
	homeBtn.normal.background = Resources.Load("Menu/P_Home");
	homeBtn.hover.background = Resources.Load("Menu/P_Home_r");
	
	playAgainBtn.normal.background = Resources.Load("Menu/P_Restart");
	playAgainBtn.hover.background = Resources.Load("Menu/P_Restart_r");
	
	resumeBtn.normal.background = Resources.Load("Menu/P_Play");
	resumeBtn.hover.background = Resources.Load("Menu/P_Play_r");
	if(!enableGUI) enableGUI = true;
	else enableGUI = false;
	currentSelected = 0;
	invaderCtrl.PauseGame();
	
}


function OnGUI()
{
	if(!enableGUI)
		return;		
	showPauseMenu();	
}

function showPauseMenu()
{
	GUI.depth = 10;
	// Make a background box
	GUI.Box(Rect(menu_pos_x,menu_pos_y,menu_box_width,menu_box_height), "",MenuBg);

	menuButtons( Rect(menu_pos_x+130,menu_pos_y+50,48,48), Rect(menu_pos_x+200,menu_pos_y+50,56,48), Rect(menu_pos_x+280,menu_pos_y+50,48,48));
	
}

function menuButtons(homeBtnRect:Rect, resumeBtnRect:Rect, playAgainBtnRect:Rect)
{
	// Make the first button. If it is pressed, Application.Loadlevel (1) will be executed
		if(GUI.Button(homeBtnRect,"",homeBtn)) {
			enableGUI = false;
			invaderCtrl.PauseGame();
			Application.LoadLevel("Menu");
		}

		// Make the second button.
		if(GUI.Button(playAgainBtnRect, "",playAgainBtn)) {	
			enableGUI = false;
			invaderCtrl.PauseGame();
			Application.LoadLevel("MainGame");
		}
		
		if(GUI.Button(resumeBtnRect, "",resumeBtn)) {	
			enableGUI = false;
			invaderCtrl.PauseGame();
			
		}
	
}