#pragma strict

var bubblePopUp:Texture;
var shieldExplosion : GameObject;
var invaderCtrl: InvaderControl ;
var _powerUpControl:powerUpControl;
var duplicatePlayer:GameObject;
var _BubbleName : String;
var _animationCtrl : BubbleFormAnimation;
var isBubblePop:boolean = false;
var bubblePopAudio : AudioClip;

function Start()
{
	invaderCtrl = GameObject.Find("_Manager").GetComponent("InvaderControl") as InvaderControl;
	_powerUpControl = GameObject.Find("_Manager").GetComponent("powerUpControl") as powerUpControl ;
	_animationCtrl = gameObject.GetComponent("BubbleFormAnimation") as BubbleFormAnimation;
	
	if(_powerUpControl.GetCurrentPlayer() == "player1")
	{
		_BubbleName = "Bubble1";
		duplicatePlayer = invaderCtrl.player;
	}
	else
	{
		_BubbleName = "Bubble2";
		duplicatePlayer =invaderCtrl.player1;
	}	
}

function DestroyBubble()
{
		gameObject.renderer.material.mainTexture = bubblePopUp;
		Destroy(gameObject.GetComponent("BubbleFormAnimation"));
		gameObject.AddComponent("BubblePopAnimation");
		var	powerUpDes : BubblePopAnimation = gameObject.GetComponent("BubblePopAnimation") as BubblePopAnimation;
		gameObject.transform.localScale += Vector3(2,1.5,0);
		isBubblePop = true;
		powerUpDes.SetFields(4,1,0.3,true);	
		var _audio :AudioSource = gameObject.GetComponent("AudioSource") as AudioSource;
		_audio.clip = bubblePopAudio;
		_audio.Play();
}

function Update()
{	
	if(duplicatePlayer)
	{
		gameObject.name = _BubbleName;
		if(invaderCtrl.gameMode != 0)
		{
			gameObject.transform.position = duplicatePlayer.transform.position;
			if(isBubblePop)
			gameObject.transform.position.y= -2.88;
		}
		gameObject.transform.position = duplicatePlayer.transform.position;
		if(isBubblePop)
			gameObject.transform.position.y= -2.88;
	}
}