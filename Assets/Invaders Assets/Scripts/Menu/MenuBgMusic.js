#pragma strict
static var instance : MenuBgMusic;
var isSound:boolean;

function Awake () {
	instance = this;
    DontDestroyOnLoad (this);
}


function Start () {
	isSound = false;
	Utility.LoadVolume();	
}


function Update () 
{		
	if(!gameObject.audio.enabled)
		return;
		
	if( (Application.loadedLevelName) == "Menu")
	{
		if(!audio.isPlaying)
			audio.Play();
	}
	if( (Application.loadedLevelName) == "MainGame")
		audio.Stop();
}



function EnableSound()
{
	isSound = true;
}