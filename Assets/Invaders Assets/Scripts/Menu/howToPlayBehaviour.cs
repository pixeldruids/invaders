using UnityEngine;
using System.Collections;

public class howToPlayBehaviour : MonoBehaviour {
		
	public AudioClip menuBackSound;
	
	// Use this for initialization
	void Start () {
			
		InputManager.Instance.greenThrottle.Reset();

	}
	
	void PlayMenuBackSound()
	{
		audio.clip = menuBackSound;
		audio.Play();
	} 
	
	// Update is called once per frame
	void Update () {
		
		if(Input.GetKeyDown(KeyCode.Escape) || InputManager.Instance.GetKey(InputManager.IB_SELECT))
		{			
			PlayMenuBackSound();
			Application.Quit();
		}	
		
		if(Input.GetKeyDown(KeyCode.Escape) || InputManager.Instance.GetKeyDown(InputManager.IB_PREV) )
		{
			PlayMenuBackSound();
			Application.LoadLevel("Menu");
		}

	}
}
