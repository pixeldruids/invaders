#pragma strict

var sharkSpeed :int = 15;
var invadersRow : int ;
var invaderCtrl : InvaderControl;
var yValue : float ;

function Start () {
	gameObject.name= "sharkpowerup";
	if(GameObject.Find("_Manager"))
		invaderCtrl = GameObject.Find("_Manager").GetComponent("InvaderControl") as InvaderControl;
	Invoke("PlayRoarMusic",2.1f);
}

function PlayRoarMusic()
{
	audio.Play();	
}

function ReadInvaderRow(_row : int)
{
	invadersRow = _row;
}

function Makeshark () 
{
	yValue = invaderCtrl.invaderYPosArray[invadersRow];
	
	// Put shark randomly on left or right side of screen
	if (Random.value < .5) 
	{
		var sharkLocation = 90.0;
		var sharkDirection = -1;
	}
	else 
	{
		sharkLocation = -30.0;
		sharkDirection = 1;
	}
	gameObject.transform.position = new Vector3(sharkLocation, yValue, 0.0);
	
	if(sharkDirection == 1)
		gameObject.transform.Rotate(new Vector3(0,180 ,0));	
		
	// If it hasn't been destroyed, keep moving it until it goes out of bounds
	while (gameObject && gameObject.transform.position.x >= -30.0 && gameObject.transform.position.x <= 90.0) 
	{
		gameObject.transform.position.x += Time.deltaTime * sharkSpeed * sharkDirection;
		yValue =  invaderCtrl.invaderYPosArray[invadersRow];
		gameObject.transform.position.y = yValue;
		yield;
	}
	if (gameObject) {   //destroy shark
		Destroy(gameObject);
	}
}



function OnTriggerEnter(other : Collider)
{

	if(other.gameObject.tag == "alien"){
		Destroy(other.gameObject);
		var _name :String = (GameObject.Find("_Manager").GetComponent("powerUpControl")as powerUpControl).GetCurrentPlayer(); 
		_name = (_name == "player1") ?"player1missile":"player2missile";
		InvaderControl.use.InvaderDestroyed (parseInt(other.gameObject.name), other.collider.transform.position, _name,true);
	}
}

