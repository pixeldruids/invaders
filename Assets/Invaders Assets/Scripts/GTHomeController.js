
var GTHomeObject:GameObject;

function Start () {
	 DontDestroyOnLoad (this);
	InputManager.Instance.greenThrottle.Reset();

}

function Update()
{
	if(Input.GetKeyDown(KeyCode.G) || (InputManager.Instance.GetKeyDown(InputManager.IB_GT)))
	{
		if(!GameObject.FindWithTag("Arena")){
			Time.timeScale =0.0;
			Utility.SetVolumeForPauseScreen(0);
			Instantiate(GTHomeObject);
		}
	}
	
	SetOrientation();
}

function OnApplicationFocus(focus : boolean) 
{
	if(Application.isLoadingLevel)
		return;
	if(Application.loadedLevel==0)
		return;
			
    if(!focus) {
  
    	Utility.SetVolumeForPauseScreen(0);
        Time.timeScale = 0;
    	if(!(GameObject.FindWithTag("Arena"))){
    		Instantiate(GTHomeObject);
 	  	}
    }
    else
    {
    	
    }
  
}


function OnApplicationPause(pause:boolean)
{
	if(pause)
	{
	}
	else
	{
		if(Application.loadedLevelName!="MainGame")
		{
			if((GameObject.FindWithTag("Arena"))){
				(GameObject.FindWithTag("Arena").GetComponent("AtlasController") as AtlasController).Hide();
			}
		}
	}
}


function SetOrientation()
{

	if (SystemInfo.deviceModel == "Amazon KFTT" || SystemInfo.deviceModel == "Amazon KFJWI" || SystemInfo.deviceModel == "Amazon KFJWA")
		{
		    if(Input.deviceOrientation == DeviceOrientation.LandscapeLeft )
		    {
		        Screen.orientation = ScreenOrientation.LandscapeRight;
		    }
		    else if(Input.deviceOrientation == DeviceOrientation.LandscapeRight )
		    {
		        Screen.orientation = ScreenOrientation.LandscapeLeft;
		    }
		}
		else
		{
		    if(Input.deviceOrientation == DeviceOrientation.LandscapeLeft )
		    {
		        Screen.orientation = ScreenOrientation.LandscapeLeft;
		    }
		    else if(Input.deviceOrientation == DeviceOrientation.LandscapeRight )
		    {
		        Screen.orientation = ScreenOrientation.LandscapeRight;
		    }
		}
}
