// Tells InvaderControl script to reverse direction of invaders if one enters the trigger area

private var triggered = false;

function OnTriggerEnter (other : Collider) {
	if (triggered || other.tag != "alien") {return;}	// Ensure that the trigger event only happens once and only for invaders
	
	triggered = true;
	InvaderControl.iDirection = -InvaderControl.iDirection;
	yield InvaderControl.use.MoveInvadersDown();
	triggered = false;
}