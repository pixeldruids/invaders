using UnityEngine;
using System.Collections;
using System.Globalization;
using System;

/******************************************************************************************************
 * This component is a singleton object that can be accessed using GreenThrottle.Instance.
 * It stores all the state of every event sent from the service to bound applications.
 * The Unifier automatically binds the service for unity and unity sendmessages are sent specifically
 * to the singleton GameObject created here.
 * 
 * GreenThrottle.Instance should be called in the opening scene in an Awake of another component
 * to guarantee all controller state is captured for the application.
 * 
 ******************************************************************************************************/
public class GreenThrottle : MonoBehaviour
{
	private const string UNIFIER_ACTIVITY_NAME = "com.greenthrottle.unitydroid.UnityInputUnifierActivity";

	//function signature for listening to the ControllerConnectedStateChange event
	// - controller is an integer between 1 and 4 specifying the controller for the event
	// - isConnected is true or false based on the message received about the controller
	public delegate void ControllerConnectedHandler(int controller, bool isConnected);

	/* to connect to this event, in the awake or start of a component add this:
	 * GreenThrottle.ControllerConnectedStateChange += this.FunctionCallback;
	 * 
	 * To remove it when the component is destroyed, do this:
	 * void OnDestroy()
	 * {
	 *   GreenThrottle.ControllerConnectedStateChange -= this.FunctionCallback;
	 * }
	 * 
	 * void FunctionCallback(int controller, bool isConnected)
	 * {
	 *   //do something with connection info
	 * }
	 */
	public static event ControllerConnectedHandler ControllerConnectedStateChange;
	
	//button indices
	public const int BUTTON_A = 0;
	public const int BUTTON_B = 1;
	public const int BUTTON_Y = 2;
	public const int BUTTON_X = 3;
	public const int BUTTON_DOWN = 4;
	public const int BUTTON_UP = 5;
	public const int BUTTON_LEFT = 6;
	public const int BUTTON_RIGHT = 7;
	public const int BUTTON_L1 = 8;
	public const int BUTTON_L2 = 9;
	public const int BUTTON_L3 = 10;
	public const int BUTTON_R1 = 11;
	public const int BUTTON_R2 = 12;
	public const int BUTTON_R3 = 13;
	public const int BUTTON_ANALOG_LEFT_UP = 14;
	public const int BUTTON_ANALOG_LEFT_DOWN = 15;
	public const int BUTTON_ANALOG_LEFT_LEFT = 16;
	public const int BUTTON_ANALOG_LEFT_RIGHT = 17;
	public const int BUTTON_ANALOG_RIGHT_UP = 18;
	public const int BUTTON_ANALOG_RIGHT_DOWN = 19;
	public const int BUTTON_ANALOG_RIGHT_LEFT = 20;
	public const int BUTTON_ANALOG_RIGHT_RIGHT = 21;
	public const int BUTTON_BACK = 22;
	public const int BUTTON_START = 23;
	public const int BUTTON_GTHOME = 24;

	private const int BUTTONS_COUNT = 25;

	public const int CONTROLLER_1 = 0;
	public const int CONTROLLER_2 = 1;
	public const int CONTROLLER_3 = 2;
	public const int CONTROLLER_4 = 3;

	//max number of controllers
	public const int MAX_CONTROLLERS = 4;

	//Button States
	//HELD - if the button is down
	//DOWN - if the button was pressed down this frame
	//UP - if the button was released up this frame
	public const int BUTTON_STATE_HELD = 0;
	public const int BUTTON_STATE_DOWN = 1;
	public const int BUTTON_STATE_UP = 2;

	private const int BUTTON_STATES_COUNT = 3;

	//Analog controls
	//LEFT - Left analog Stick, x and y axis information ( -127 to 127, 0 centered)
	//RIGHT - Right analog Stick, x and y axis information ( -127 to 127, 0 centered)
	//L2 - Left Trigger, x axis info  ( 0 to 255 (fully engaged))
	//R2 - Right Trigger, x axis info ( 0 to 255 (fully engaged))
	public const int ANALOG_LEFT = 0;
	public const int ANALOG_RIGHT = 1;
	public const int ANALOG_L2 = 2;
	public const int ANALOG_R2 = 3;

	private const int ANALOG_AXES_COUNT = 4;

	//Analog data
	//ANALOG_X - X Data for dual axis controls, only value for single axis.
	//ANALOG_Y - Y Data for dual axis controls.  Not used for single axis.
	public const int ANALOG_X = 0;
	public const int ANALOG_Y = 1;

	private const int ANALOG_DATA_COUNT = 2;
	
	//Button State, [ButtonState, controllerId, Button] -> bool true if down
	public bool[, ,] buttonState;
	//Analog control State [AnalogDataAxis, controllerId, AnalogControl] -> float axis value from last update
	public float[,,] analogState;
	//Analog state change [controllerId, AnalogControl] -> bool specifies if the analog value changed this frame
	public bool[,] analogChanged;
	//Controller Connection State [controller] -> bool true if controller is connected
	public bool[] connectedState;

	//If there was any activity from the service yet.  Activity always connects service so it's to detect if controllers
	//are sending data to the game
	public bool isServiceActive = false;

	void Awake()
	{
		if (instance != null)
		{
			Debug.LogError("Destroying GreenThrottle object that was created after singleton, use GreenThrottle.Instance");
			Destroy(this.gameObject);
			return;
		}

		buttonState = new bool[BUTTON_STATES_COUNT, MAX_CONTROLLERS, BUTTONS_COUNT];
		analogState = new float[ANALOG_DATA_COUNT, MAX_CONTROLLERS, ANALOG_AXES_COUNT];
		analogChanged = new bool[MAX_CONTROLLERS, ANALOG_AXES_COUNT];

		connectedState = new bool[MAX_CONTROLLERS];

		for (int i = 0; i < BUTTON_STATES_COUNT; ++i)
		{
			for (int j = 0; j < MAX_CONTROLLERS; ++j)
			{
				for (int k = 0; k < BUTTONS_COUNT; ++k)
				{
					buttonState[i, j, k] = false;
				}
			}
		}

		for (int i = 0; i < ANALOG_DATA_COUNT; ++i)
		{
			for (int j = 0; j < MAX_CONTROLLERS; ++j)
			{
				for (int k = 0; k < ANALOG_AXES_COUNT; ++k)
				{
					analogState[i, j, k] = 0f;
				}
			}
		}

		for (int i = 0; i < MAX_CONTROLLERS; ++i)
		{
			connectedState[i] = false;
			for (int j = 0; j < ANALOG_AXES_COUNT; ++j)
			{
				analogChanged[i,j] = false;
			}
		}
	}

	void OnDestroy()
	{
		if (instance == this)
		{
			Debug.Log("Destroying GreenThrottle.Instance");
			instance = null;
			Destroy(singletonGO);
			singletonGO = null;
			ControllerConnectedStateChange = null;
		}
	}

	void OnApplicationQuit()
	{
		if (instance != null)
		{
			Destroy(instance);
		}
	}
	
	
	public void Reset ()
	{
		for (int i = 0; i < BUTTON_STATES_COUNT; ++i)
		{
			for (int j = 0; j < MAX_CONTROLLERS; ++j)
			{
				for (int k = 0; k < BUTTONS_COUNT; ++k)
				{
					buttonState[i, j, k] = false;
				}
			}
		}

		for (int i = 0; i < ANALOG_DATA_COUNT; ++i)
		{
			for (int j = 0; j < MAX_CONTROLLERS; ++j)
			{
				for (int k = 0; k < ANALOG_AXES_COUNT; ++k)
				{
					analogState[i, j, k] = 0f;
				}
			}
		}

		for (int i = 0; i < MAX_CONTROLLERS; ++i)
		{
			connectedState[i] = false;
			for (int j = 0; j < ANALOG_AXES_COUNT; ++j)
			{
				analogChanged[i,j] = false;
			}
		}

	}

	
	public void LateUpdate()
	{
		for (int j = 0; j < MAX_CONTROLLERS; ++j)
		{
			for( int k = 0 ; k < BUTTONS_COUNT ; ++k )
			{
				buttonState[BUTTON_STATE_DOWN, j, k] = false;
				buttonState[BUTTON_STATE_UP, j, k] = false;
			}

			for( int k = 0; k < ANALOG_AXES_COUNT; ++k)
			{
				analogChanged[j, k] = false;
			}
		}
	}
	
	private void ButtonResponse( int button, int controller, bool down )
	{
		buttonState[BUTTON_STATE_UP, controller, button] = !down;
		buttonState[BUTTON_STATE_HELD, controller, button] = down;
		buttonState[BUTTON_STATE_DOWN, controller, button] = down;
	}
	
	private void SetButton(string encodedVal, int controller, bool down)
	{
		if (controller >= MAX_CONTROLLERS)
		{
			return;
		}

		switch( encodedVal )
		{
			case "A": ButtonResponse(BUTTON_A, controller, down);				break;
			case "B": ButtonResponse(BUTTON_B, controller, down); break;
			case "Y": ButtonResponse(BUTTON_Y, controller, down); break;
			case "X": ButtonResponse(BUTTON_X, controller, down); break;
			case "D": ButtonResponse(BUTTON_DOWN, controller, down); break;
			case "U": ButtonResponse(BUTTON_UP, controller, down); break;
			case "L": ButtonResponse(BUTTON_LEFT, controller, down); break;
			case "R": ButtonResponse(BUTTON_RIGHT, controller, down); break;
			case "L1": ButtonResponse(BUTTON_L1, controller, down); break;
			case "L2": ButtonResponse(BUTTON_L2, controller, down); break;
			case "L3": ButtonResponse(BUTTON_L3, controller, down); break;
			case "R1": ButtonResponse(BUTTON_R1, controller, down); break;
			case "R2": ButtonResponse(BUTTON_R2, controller, down); break;
			case "R3": ButtonResponse(BUTTON_R3, controller, down); break;
			case "ST": ButtonResponse(BUTTON_START, controller, down); break;
			case "SEL": ButtonResponse(BUTTON_BACK, controller, down); break;
			case "GT": ButtonResponse(BUTTON_GTHOME, controller, down); break;
			case "LAD": ButtonResponse(BUTTON_ANALOG_LEFT_DOWN, controller, down); break;
			case "LAU": ButtonResponse(BUTTON_ANALOG_LEFT_UP, controller, down); break;
			case "LAL": ButtonResponse(BUTTON_ANALOG_LEFT_LEFT, controller, down); break;
			case "LAR": ButtonResponse(BUTTON_ANALOG_LEFT_RIGHT, controller, down); break;
			case "RAD": ButtonResponse(BUTTON_ANALOG_RIGHT_DOWN, controller, down); break;
			case "RAU": ButtonResponse(BUTTON_ANALOG_RIGHT_UP, controller, down); break;
			case "RAL": ButtonResponse(BUTTON_ANALOG_RIGHT_LEFT, controller, down); break;
			case "RAR": ButtonResponse(BUTTON_ANALOG_RIGHT_RIGHT, controller, down); break;
		}
	}

	//sets a button press. I.E. button is down
	//string format (C)(CODE) where (C) is controllerId and (CODE) is the string code for the button
	public void set_Button(string encodedVal)
	{
		SetButton(encodedVal.Substring(1), int.Parse(encodedVal.Substring (0,1)) - 1, true);
	}
	//Clears a button press. I.E. button is up
	//string format (C)(CODE) where (C) is controllerId and (CODE) is the string code for the button
	public void clear_Button(string encodedVal)
	{
		SetButton(encodedVal.Substring(1), int.Parse(encodedVal.Substring(0, 1)) - 1, false);
	}
	
	//Analog event callback
	//string format is (C),(S),(X),(Y) where (C) is controllerId, (S) is which analog control it is
	//(X) is the X component of dual axis controls or just the value of the single axis controls
	//(Y) is the Y component of dual axis controls.  Single Axis controls do not send this value
	public void analog_Event(string encodedVal)
	{
		string[] vals = encodedVal.Split(',');
		int controller = int.Parse(vals[0]) - 1;

		if (controller >= MAX_CONTROLLERS)
		{
			return;
		}

		switch (vals[1])
		{
			case "LA":
				{
					analogState[ANALOG_X, controller, ANALOG_LEFT] = float.Parse(vals[2]);
					analogState[ANALOG_Y, controller, ANALOG_LEFT] = float.Parse(vals[3]);
					analogChanged[controller, ANALOG_LEFT] = true;
				}
				break;
			case "RA":
				{
					analogState[ANALOG_X, controller, ANALOG_RIGHT] = float.Parse(vals[2]);
					analogState[ANALOG_Y, controller, ANALOG_RIGHT] = float.Parse(vals[3]);
					analogChanged[controller, ANALOG_RIGHT] = true;
				}
				break;
			case "L2A":
				{
					analogState[ANALOG_X, controller, ANALOG_L2] = float.Parse(vals[2]);
					analogChanged[controller, ANALOG_L2] = true;
				}
				break;
			case "R2A":
				{
					analogState[ANALOG_X, controller, ANALOG_R2] = float.Parse(vals[2]);
					analogChanged[controller, ANALOG_R2] = true;
				}
				break;
		}
	}

	//Controller Connection State callback.  Whenever a controller connects or disconnects, this is called
	//string format is (A)X(Y) where (A) is the controllerId and
	//(Y) is the character C or D, whether controller is the connected or disconnected
	public void controller_Event( string encodedVal )
	{
		int controller = int.Parse(encodedVal.Substring(0, 1)) - 1;

		if (controller >= MAX_CONTROLLERS)
		{
			return;
		}

		bool previousState = connectedState[controller];

		string stateString = encodedVal.Substring(1);
		if (stateString == "XC")
		{
			connectedState[controller] = true;
		}
		else if (stateString == "XD")
		{
			connectedState[controller] = false;
		}

		if(previousState != connectedState[controller] && ControllerConnectedStateChange != null)
		{
			ControllerConnectedStateChange(controller+1, connectedState[controller]);
		}
		
	}

	public void service_Event(String isConnected)
	{
		isServiceActive = Boolean.Parse(isConnected);
	}

	// Allows the game to clear controller state for a specific controller.  
	// This should be used on disconnect to clear all down buttons and restore the input to a neutral state.
	public void ClearControllerState( int controller )
	{
		--controller;
		
		for( int i = 0 ; i < BUTTON_STATES_COUNT ; ++i )
		{
			for( int k = 0 ; k < BUTTONS_COUNT ; ++k )
			{
				buttonState[i, controller, k] = false;
			}
		}

		for (int i = 0; i < ANALOG_DATA_COUNT; ++i)
		{
			for (int k = 0; k < ANALOG_AXES_COUNT; ++k)
			{
				analogState[i, controller, k] = 0f;
			}
		}	
	}

	//remaps left analog to dpad internally to unifier, on by default
	public void addLeftButtonRemap()
	{
#if UNITY_ANDROID && !UNITY_EDITOR
		AndroidJavaClass jc = new AndroidJavaClass(UNIFIER_ACTIVITY_NAME);
		jc.CallStatic("addLeftButtonRemap");
#endif
	}

	//removes left analog to dpad remap
	public void removeLeftButtonRemap()
	{
#if UNITY_ANDROID && !UNITY_EDITOR
		AndroidJavaClass jc = new AndroidJavaClass(UNIFIER_ACTIVITY_NAME);
		jc.CallStatic("removeLeftButtonRemap");
#endif
	}

	//remaps right analog to abyx buttons according to their cardinal relationship. off by default
	public void addRightButtonRemap()
	{
#if UNITY_ANDROID && !UNITY_EDITOR
		AndroidJavaClass jc = new AndroidJavaClass(UNIFIER_ACTIVITY_NAME);
		jc.CallStatic("addRightButtonRemap");
#endif
	}

	//removes right analog to abyx remap
	public void removeRightButtonRemap()
	{
#if UNITY_ANDROID && !UNITY_EDITOR
		AndroidJavaClass jc = new AndroidJavaClass(UNIFIER_ACTIVITY_NAME);
		jc.CallStatic("removeRightButtonRemap");
#endif
	}

	//attempts to hide the on screen system UI bar that pops up in ICS or newer android devices
	public void hideAndroidSystemUI()
	{
#if UNITY_ANDROID && !UNITY_EDITOR
		AndroidJavaClass jc = new AndroidJavaClass(UNIFIER_ACTIVITY_NAME);
		jc.CallStatic("hideAndroidSystemUI");
#endif
	}

	//removes the sensor event listener from the unity player so that 
	//accelerometer events don't get sent to the app.
	//This can be a slight performance increase, if your app doesn't require
	//any Sensor inputs.
	public void disableAccelerometer()
	{
#if UNITY_ANDROID && !UNITY_EDITOR
		AndroidJavaClass jc = new AndroidJavaClass(UNIFIER_ACTIVITY_NAME);
		jc.CallStatic("disableAccelerometer");
#endif
	}

	//Stops attempting to disable the SensorEventListener on the UnityPlayer
	//It will not automatically be activated.  THe game will need to be
	//paused in android and resumed for this to work.  It is on by default
	public void enableAccelerometer()
	{
#if UNITY_ANDROID && !UNITY_EDITOR
		AndroidJavaClass jc = new AndroidJavaClass(UNIFIER_ACTIVITY_NAME);
		jc.CallStatic("enableAccelerometer");
#endif
	}


	private static GreenThrottle instance = null;
	private static GameObject singletonGO = null;
	public static GreenThrottle Instance
	{
		get
		{
			if (instance == null)
			{
				Debug.Log("Creating GreenThrottle Interface");
				singletonGO = new GameObject();
				instance = singletonGO.AddComponent<GreenThrottle>();
				singletonGO.name = "GreenThrottleSingleton";
				DontDestroyOnLoad(singletonGO);
			}

			return instance;
		}
	}
	
/*	void OnGUI()
	{
		string buttonsStr = "";
		for (int j = 0; j < MAX_CONTROLLERS; ++j)
		{
			for( int k = 0 ; k < BUTTONS_COUNT ; ++k )
			{
				if(buttonState[BUTTON_STATE_DOWN, j, k])
				{
					buttonsStr += k.ToString() + " ";
				}
			}


		}
		GUI.Label(new Rect(150,150,100,100),buttonsStr);
	}
*/
	
	
}
