using UnityEngine;
using System.Collections;
using System.Globalization;

public class InputManager : MonoBehaviour
{
	
	//input axes
	public const int IA_X = 1;
	public const int IA_Y = 2;
	public const int IA_FIREX = 3;
	public const int IA_FIREY = 4;
	public const int IA_X_ANALOG = 5;
	public const int IA_Y_ANALOG = 6;
	
	//input buttons
	public const int IB_LEFT = 1;
	public const int IB_RIGHT = 2;
	public const int IB_FORWARD = 3;
	public const int IB_BACK = 4;
	public const int IB_MOVEMENT = 5;
	public const int IB_FIRE = 6;
	public const int IB_SELECT = 7;
	public const int IB_START = 8;
	public const int IB_L2 = 9;
	public const int IB_R2 = 10;
	public const int IB_PREV = 11;
	public const int IB_GT = 24;
	
	
	public GreenThrottle greenThrottle;

	private const int MAX_PLAYERS = 2;

	public const int ANALOG_X = 0;
	public const int ANALOG_Y = 1;
	public const int ANALOG_AXES = 2;

	//analog data
	private float[,] analogValue = new float[MAX_PLAYERS, ANALOG_AXES];
	private bool[] analogComputed = new bool[MAX_PLAYERS];
	
	//analog computations
	private const float analogMaxRadius = 120f;
	private const float analogMaxRadiusSq = analogMaxRadius * analogMaxRadius;
	private const float analogMaxRadiusRecip = 1f / analogMaxRadius;
	private const float analogCardinalWidth = 25f;
	private const float analogDiagonalWidth = 15.9f;	// = sqrt((.9*analogCardinalWidth)^2 / 2), length of triangle side where len(hyp) = .9*analogCardinalWidth (full width seemed too wide)
	private const float analogTriggerMax = 255f;
	private const float analogTriggerMaxRecip = 1f / analogTriggerMax;
	
	public static int gameMode;
	
	public GameObject GTPrefab;
	
	// Use this for initialization
	void Awake()
	{
		if (instance != null)
		{
			Debug.LogError("Destroying InputManager that was created after singleton, use InputManager.Instance");
			Destroy(gameObject);
			return;
		}
		
		greenThrottle = GreenThrottle.Instance;
		greenThrottle.disableAccelerometer();
		enabled = false;

#if UNITY_IPHONE || UNITY_ANDROID
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
#endif

		for (int i = 0; i < MAX_PLAYERS; ++i)
		{
			
			for( int j = 0 ; j < ANALOG_AXES ; ++j )
			{
				analogValue[i,j] = 0f;
			}
			analogComputed[i] = false;
		}
	}
	
	public float GetAxis( int axis, int player )
	{
		float val = 0f;

		switch( axis )
		{
			case(IA_X):
				return (greenThrottle.buttonState[GreenThrottle.BUTTON_STATE_HELD, player - 1, GreenThrottle.BUTTON_LEFT] || greenThrottle.buttonState[GreenThrottle.BUTTON_STATE_HELD, player - 1, GreenThrottle.BUTTON_ANALOG_LEFT_LEFT] ? 1 : 0)
					+ (greenThrottle.buttonState[GreenThrottle.BUTTON_STATE_HELD, player - 1, GreenThrottle.BUTTON_RIGHT] || greenThrottle.buttonState[GreenThrottle.BUTTON_STATE_HELD, player - 1, GreenThrottle.BUTTON_ANALOG_LEFT_RIGHT] ? -1 : 0);
			case IA_X_ANALOG:
				ComputeAnalogStick(player-1);
				val = -analogValue[player - 1, ANALOG_X] + (greenThrottle.buttonState[GreenThrottle.BUTTON_STATE_HELD, player - 1, GreenThrottle.BUTTON_LEFT] ? 1f : 0f) + (greenThrottle.buttonState[GreenThrottle.BUTTON_STATE_HELD, player - 1, GreenThrottle.BUTTON_RIGHT] ? -1f : 0f);
				return Mathf.Min( Mathf.Max(val, -1f), 1f);
			case (IA_Y):
				{
					return ((greenThrottle.buttonState[GreenThrottle.BUTTON_STATE_HELD, player - 1, GreenThrottle.BUTTON_DOWN] ||
							 greenThrottle.buttonState[GreenThrottle.BUTTON_STATE_HELD, player - 1, GreenThrottle.BUTTON_ANALOG_LEFT_DOWN]) ? 1 : 0)
						+ ((greenThrottle.buttonState[GreenThrottle.BUTTON_STATE_HELD, player - 1, GreenThrottle.BUTTON_UP] ||
							greenThrottle.buttonState[GreenThrottle.BUTTON_STATE_HELD, player - 1, GreenThrottle.BUTTON_ANALOG_LEFT_UP]) ? -1 : 0);
				}
			case IA_Y_ANALOG:
				ComputeAnalogStick(player-1);	//always compute from first axis
				val = -analogValue[player - 1, ANALOG_Y] + (greenThrottle.buttonState[GreenThrottle.BUTTON_STATE_HELD, player - 1, GreenThrottle.BUTTON_DOWN] ? 1f : 0f) + (greenThrottle.buttonState[GreenThrottle.BUTTON_STATE_HELD, player - 1, GreenThrottle.BUTTON_UP] ? -1f : 0f);
				return Mathf.Min(Mathf.Max(val, -1f), 1f);
			case(IA_FIREX):
				return (greenThrottle.buttonState[GreenThrottle.BUTTON_STATE_HELD, player - 1, GreenThrottle.BUTTON_ANALOG_RIGHT_LEFT] ? 1 : 0) +
					   (greenThrottle.buttonState[GreenThrottle.BUTTON_STATE_HELD, player - 1, GreenThrottle.BUTTON_ANALOG_RIGHT_RIGHT] ? -1 : 0);
			case(IA_FIREY):
				return (greenThrottle.buttonState[GreenThrottle.BUTTON_STATE_HELD, player - 1, GreenThrottle.BUTTON_ANALOG_RIGHT_UP] ? -1 : 0) +
					   (greenThrottle.buttonState[GreenThrottle.BUTTON_STATE_HELD, player - 1, GreenThrottle.BUTTON_ANALOG_RIGHT_DOWN] ? 1 : 0);
		}
		return 0;
	}
	
	
	
	private bool GetKey( int type, int button, int player )
	{

		switch( button )
		{
			case(IB_LEFT):
				return greenThrottle.buttonState[type, player - 1, GreenThrottle.BUTTON_LEFT];
			case(IB_RIGHT):
				return greenThrottle.buttonState[type, player - 1, GreenThrottle.BUTTON_RIGHT];
			case(IB_MOVEMENT):
				return greenThrottle.buttonState[type, player - 1, GreenThrottle.BUTTON_LEFT] || greenThrottle.buttonState[type, player - 1, GreenThrottle.BUTTON_RIGHT]
					|| greenThrottle.buttonState[type, player - 1, GreenThrottle.BUTTON_UP] || greenThrottle.buttonState[type, player - 1, GreenThrottle.BUTTON_DOWN]
					|| greenThrottle.buttonState[type, player - 1, GreenThrottle.BUTTON_ANALOG_LEFT_LEFT] || greenThrottle.buttonState[type, player - 1, GreenThrottle.BUTTON_ANALOG_LEFT_RIGHT]
					|| greenThrottle.buttonState[type, player - 1, GreenThrottle.BUTTON_ANALOG_LEFT_UP] || greenThrottle.buttonState[type, player - 1, GreenThrottle.BUTTON_ANALOG_LEFT_DOWN];
			case(IB_FORWARD):
				return greenThrottle.buttonState[type, player - 1, GreenThrottle.BUTTON_UP] || greenThrottle.buttonState[type, player - 1, GreenThrottle.BUTTON_ANALOG_LEFT_UP];
			case(IB_BACK):
				return greenThrottle.buttonState[type, player - 1, GreenThrottle.BUTTON_DOWN] || greenThrottle.buttonState[type, player - 1, GreenThrottle.BUTTON_ANALOG_LEFT_DOWN];
			case(IB_FIRE):
				return  greenThrottle.buttonState[type, player - 1, GreenThrottle.BUTTON_A] ;
			case(IB_PREV):
				return  greenThrottle.buttonState[type, player - 1, GreenThrottle.BUTTON_B] ;			
			case (IB_SELECT):
				return greenThrottle.buttonState[type, player - 1, GreenThrottle.BUTTON_BACK];
			case(IB_START):
				return greenThrottle.buttonState[type, player - 1, GreenThrottle.BUTTON_START];
			case(IB_GT):
				return greenThrottle.buttonState[type, player - 1, GreenThrottle.BUTTON_GTHOME];
			case(IB_R2):
				return greenThrottle.buttonState[type, player - 1, GreenThrottle.BUTTON_R2];
		}
		return false;
	}
	
	public bool GetKey( int button, int player )
	{
		return GetKey(GreenThrottle.BUTTON_STATE_HELD, button, player);
	}
	
	public bool GetKey( int button )
	{
		bool val = false;
		for( int i = 1 ; !val && i <= MAX_PLAYERS ; ++i )
		{
			val |= GetKey(button, i);
		}
		return val;
	}
	
	public bool GetKeyDown( int button, int player )
	{
		return GetKey(GreenThrottle.BUTTON_STATE_DOWN, button, player);
	}

	public bool GetKeyDown(int button)
	{
		bool val = false;
		for( int i = 1 ; !val && i <= MAX_PLAYERS ; ++i )
		{
			val |= GetKeyDown(button, i);
		}
		return val;
	}
	
	public void ClearController( int player )
	{
		greenThrottle.ClearControllerState(player);
		--player;
		
		for( int i = 0 ; i < ANALOG_AXES; ++i )
		{
			analogValue[player, i] = 0f;
		}
		analogComputed[player] = true;
	}

	//analog input
	private void ComputeAnalogStick( int player )
	{
		if (analogComputed[player] && !greenThrottle.analogChanged[player, GreenThrottle.ANALOG_LEFT])
		{
			return;
		}

		float x = greenThrottle.analogState[GreenThrottle.ANALOG_X, player, GreenThrottle.ANALOG_LEFT];
		float y = greenThrottle.analogState[GreenThrottle.ANALOG_Y, player, GreenThrottle.ANALOG_LEFT];
		float val;
		
		//bool reportOutput = false;
		
		//if applicable, snap axis values to a direction
		if( x > -analogCardinalWidth && x < analogCardinalWidth )
		{
			x = 0f;
		}
		if( y > -analogCardinalWidth && y < analogCardinalWidth )
		{
			y = 0f;
		}
		if( x != 0f && y != 0f )
		{
			//check diagonals
			if( (x - y) > -analogDiagonalWidth && (x - y) < analogDiagonalWidth )
			{
				//Debug.Log("anin: found positive diagonal from x: " + x + "  y: " + y);
				
				//snap to the diagonal axis, take the average of the values
				x = (x + y) * .5f;
				y = x;
			}
			else if( (x + y) > -analogDiagonalWidth && (x + y) < analogDiagonalWidth )
			{
				//Debug.Log("anin: found negative diagonal from x: " + x + "  y: " + y);

				//snap to the negative diagonal axis, average with the negative
				x = (x - y) * .5f;
				y = -x;
			}
			/*
			else
			{
				//Debug.Log("anin: found neither diagonal from x: " + x + "  y: " + y);
				reportOutput = true;
			}
			*/
		}
		
		//normalize the new values from [-1,1]
		val = (x * x) + (y * y);
		if( val > analogMaxRadiusSq )
		{
			val = 1f / Mathf.Sqrt(val);
			x *= val;
			y *= val;
		}
		else
		{
			x *= analogMaxRadiusRecip;
			y *= analogMaxRadiusRecip;
		}
		
		analogValue[player, ANALOG_X] = x;
		analogValue[player, ANALOG_Y] = y;
		
		analogComputed[player] = true;
		analogComputed[player] = true;
		/*
		if( reportOutput )
		{
			Debug.Log("anin: final output x: " + x + "  y: " + y);
		}
		*/
	}

	private static InputManager instance = null;
	private static GameObject singletonGO = null;
	public static InputManager Instance
	{
		get
		{
			if (instance == null)
			{
				singletonGO = new GameObject();
				instance = singletonGO.AddComponent<InputManager>();
				singletonGO.name = "InputManager";
				DontDestroyOnLoad(singletonGO);
			}

			return instance;
		}
	}
	
	
	

	
		
}
